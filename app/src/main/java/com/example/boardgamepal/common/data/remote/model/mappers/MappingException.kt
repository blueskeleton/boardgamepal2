package com.example.boardgamepal.common.data.remote.model.mappers

class MappingException(message: String): Exception(message)