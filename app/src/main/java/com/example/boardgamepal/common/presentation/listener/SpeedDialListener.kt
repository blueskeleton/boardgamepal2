package com.example.boardgamepal.common.presentation.listener

interface SpeedDialListener {
  fun addGameToCollection()
  fun deleteGameFromCollection()
  fun showListsToAdd()
  fun showListsToDelete()
}