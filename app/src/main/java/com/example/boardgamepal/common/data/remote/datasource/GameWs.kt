package com.example.boardgamepal.common.data.remote.datasource

import com.example.boardgamepal.common.data.remote.model.GameDetailResponseDTO
import com.example.boardgamepal.common.data.remote.model.GameResponseDTO
import com.example.boardgamepal.common.data.remote.utils.ApiConstants
import com.example.boardgamepal.common.data.remote.utils.ApiParameters
import retrofit2.http.GET
import retrofit2.http.Query

interface GameWs {

  @GET(ApiConstants.SEARCH_ENDPOINT)
  suspend fun getTopGames(
    @Query(ApiParameters.CLIENT_ID) clientId: String,
    @Query(ApiParameters.ORDER_BY) orderBy: String,
    @Query(ApiParameters.SKIP) skip: Int
  ): GameResponseDTO

  @GET(ApiConstants.SEARCH_ENDPOINT)
  suspend fun getGamesSearch(
    @Query(ApiParameters.CLIENT_ID) clientId: String,
    @Query(ApiParameters.NAME) name: String,
    @Query(ApiParameters.SKIP) skip: Int
  ): GameResponseDTO

  @GET(ApiConstants.DETAIL_ENDPOINT)
  suspend fun getGameDetail(
    @Query(ApiParameters.CLIENT_ID) clientId: String,
    @Query(ApiParameters.GAME_ID) gameId: String,
  ): GameDetailResponseDTO

}