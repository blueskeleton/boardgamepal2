package com.example.boardgamepal.common.presentation.widget

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import com.example.boardgamepal.R
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.common.utils.extension.getLifecycleOwner
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.databinding.ViewExpandableTextBinding

class ExpandableTextView @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

  private var binding: ViewExpandableTextBinding? = null

  var htmlText: String? = null
    set(value) {
      field = value
      binding?.htmlText = value
      binding?.executePendingBindings()
    }

  var openText: ObservableBoolean = ObservableBoolean(false)

  init {
    if (isInEditMode) {
      inflate(context, R.layout.view_expandable_text, this)
    } else {
      binding = ViewExpandableTextBinding.inflate(getLayoutInflater(), this, true)
    }
    binding?.lifecycleOwner = context.getLifecycleOwner()
    binding?.toggleText = openText
  }

  object ExpandableTextBinding {
    @BindingAdapter("bind:toggleText")
    @JvmStatic
    fun toggleText(view: View, openText: ObservableBoolean?) {
      if (openText != null && !view.hasOnClickListeners()) {
        view.setOnClickListener {
          openText.set(openText.get().not())
        }
      }
    }

    @BindingAdapter("bind:toggleRead")
    @JvmStatic
    fun toggleRead(view: TextView, openText: ObservableBoolean?) {
      val newText = if (openText?.get().isTrue()) {
        R.string.read_less
      } else {
        R.string.read_more
      }
      view.setText(newText)
    }

    @BindingAdapter("bind:toggleDescription")
    @JvmStatic
    fun toggleDescription(view: TextView, openText: ObservableBoolean?) {
      if (openText != null) {
        if (openText.get()) {
          view.background = null
        } else {
          view.setBackgroundResource(R.drawable.bg_collapsed_text)
        }
        val newMaxLines = if (openText.get()) view.lineCount else 3
        val animation = ObjectAnimator.ofInt(view, "maxLines", newMaxLines)
        animation.setDuration(200).start()
      }
    }
  }
}