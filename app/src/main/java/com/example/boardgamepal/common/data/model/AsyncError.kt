package com.example.boardgamepal.common.data.model

sealed class AsyncError(open val debugMessage: String) {
  data class ConnectionError(override val debugMessage: String) : AsyncError(debugMessage)
  data class ServerError(val code: Int, override val debugMessage: String) : AsyncError(debugMessage)
  data class DataParseError(override val debugMessage: String) : AsyncError(debugMessage)
  data class UnknownError(override val debugMessage: String, val errorThrown: Throwable) : AsyncError(debugMessage)
  data class CustomError(override val debugMessage: String) : AsyncError(debugMessage)
}