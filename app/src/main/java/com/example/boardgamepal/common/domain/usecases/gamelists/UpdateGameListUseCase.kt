package com.example.boardgamepal.common.domain.usecases.gamelists

import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class UpdateGameListUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    list: GameList
  ) = gameRepository.updateGameList(list).valueAsync().await()
}