package com.example.boardgamepal.common.presentation.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsMultiChoice
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.utils.DialogState
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.presentation.viewmodel.GameDetailViewModel
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.common.utils.extension.setFavoriteIcon
import com.example.boardgamepal.databinding.FragmentGameDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameDetailFragment : CommonBaseFragment() {

  private val args: GameDetailFragmentArgs by navArgs()
  private val viewModel: GameDetailViewModel by viewModels()
  private lateinit var binding: FragmentGameDetailBinding

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.detail_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    binding = FragmentGameDetailBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      detailViewModel = viewModel
    }

    return binding.root
  }

  override fun initializeView() {
    viewModel.initializeDetail(args.gameId)
    viewModel.getIsGameInWishListLiveData().observe(viewLifecycleOwner) {
      menu?.findItem(R.id.action_favorite).setFavoriteIcon(it)
    }
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)
    menu.findItem(R.id.action_favorite).apply {
      isVisible = viewModel.isGameDetailLoaded()
      setFavoriteIcon(viewModel.getIsGameInWishListLiveData().value.isTrue())
    }
  }

  override fun createDialog(type: DialogState) {
    when (type) {
      DialogState.SHOW_LISTS_TO_ADD -> showAddToListsDialog()
      DialogState.SHOW_LISTS_TO_REMOVE -> showDeleteFromListsDialog()
    }
  }

  private fun showAddToListsDialog() {
    viewModel.getListsWithoutGameLiveData().value?.takeIf { it.isNotEmpty() }?.let {
      MaterialDialog(requireContext()).show {
        title(R.string.select_from_lists)
        listItemsMultiChoice(items = it.map { it.name }) { _, index, _ ->
          viewModel.addGameToLists(index.toList())
        }
        positiveButton(R.string.add)
        negativeButton(R.string.cancel)
      }
    }
  }

  private fun showDeleteFromListsDialog() {
    viewModel.getListsWithThisGameLiveData().value?.takeIf { it.isNotEmpty() }?.let {
      MaterialDialog(requireContext()).show {
        title(R.string.select_from_lists)
        listItemsMultiChoice(items = it.map { it.name }) { _, index, _ ->
          viewModel.deleteGameFromLists(index.toList())
        }
        positiveButton(R.string.delete)
        negativeButton(R.string.cancel)
      }
    }
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_favorite -> {
        viewModel.onFavoriteClicked()
        true
      }
      else -> super.onMenuItemSelected(item)
    }
  }

}