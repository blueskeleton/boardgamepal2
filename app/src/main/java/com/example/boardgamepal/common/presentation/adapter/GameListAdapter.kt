package com.example.boardgamepal.common.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.presentation.adapter.holder.GameHoldersFactory
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel

class GameListAdapter(
  private val viewModel: CommonBaseViewModel,
  private val holdersFactory: GameHoldersFactory
) : ListAdapter<Game, RecyclerView.ViewHolder>(GamesListDiffCallback()) {

  //region Adapter
  override fun getItemViewType(position: Int): Int {
    return holdersFactory.getItemViewType(getItem(position))
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    val parameters = GameHoldersFactory.GameListHoldersFactoryParameters(parent, viewType, viewModel)
    return holdersFactory.onCreateViewHolder(parameters)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    holdersFactory.onBindViewHolder(holder, getItem(position))
  }
  //endregion

}

//region DiffCallback
private class GamesListDiffCallback : DiffUtil.ItemCallback<Game>() {

  override fun areItemsTheSame(oldItem: Game, newItem: Game): Boolean =
    oldItem.id == newItem.id

  override fun areContentsTheSame(oldItem: Game, newItem: Game): Boolean = false

}
//endregion