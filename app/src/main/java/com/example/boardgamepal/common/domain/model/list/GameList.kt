package com.example.boardgamepal.common.domain.model.list

data class GameList(
  val id: Long,
  val name: String
)
