package com.example.boardgamepal.common.presentation.utils

import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.example.boardgamepal.R
import com.example.boardgamepal.common.utils.extension.setCustomLabel
import com.leinardi.android.speeddial.SpeedDialActionItem

object SpeedDialManager {

  fun View.getAddToCollectionAction(): SpeedDialActionItem {
    return SpeedDialActionItem.Builder(
      R.id.fab_add_to_collection,
      R.drawable.ic_add_to_collection
    )
      .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.white, null))
      .setCustomLabel(R.string.add_to_collection)
      .create()
  }

  fun View.getDeleteFromCollectionAction(): SpeedDialActionItem {
    return SpeedDialActionItem.Builder(
      R.id.fab_delete_from_collection,
      R.drawable.ic_remove_from_collection
    )
      .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.white, null))
      .setCustomLabel(R.string.remove_from_collection)
      .create()
  }

  fun View.getAddToListsAction(): SpeedDialActionItem {
    return SpeedDialActionItem.Builder(
      R.id.fab_add_to_lists,
      R.drawable.ic_add_to_lists
    )
      .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.white, null))
      .setCustomLabel(R.string.add_to_lists)
      .create()
  }

  fun View.getDeleteFromListsAction(): SpeedDialActionItem {
    return SpeedDialActionItem.Builder(
      R.id.fab_delete_from_lists,
      R.drawable.ic_delete_from_lists
    )
      .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.white, null))
      .setCustomLabel(R.string.delete_from_lists)
      .create()
  }

}