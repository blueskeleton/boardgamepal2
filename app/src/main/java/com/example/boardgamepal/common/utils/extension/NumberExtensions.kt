package com.example.boardgamepal.common.utils.extension

import com.example.boardgamepal.common.data.remote.utils.ApiConstants
import kotlin.math.min

fun Int.orDefaultPage(): Int {
  return min(this, ApiConstants.DEFAULT_PAGE_SIZE)
}