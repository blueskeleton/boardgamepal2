package com.example.boardgamepal.common.domain.model.game

import java.math.BigDecimal
import java.math.RoundingMode

const val WRONG_RATING = -1f
const val WRONG_RATING_D = -1.0

data class Rating(
  val averageUserRating: Float
) {

  companion object {
    val DEFAULT = Rating(WRONG_RATING)
  }

  private val decimalRating: BigDecimal by lazy { averageUserRating.toBigDecimal() }

  fun contains(min: Float?, max: Float?) = averageUserRating in (min ?: 0f)..(max ?: 10f)

  val isValid by lazy { averageUserRating != WRONG_RATING }
  val ratingText by lazy { decimalRating.setScale(1, RoundingMode.HALF_EVEN).toString() }
  val integerValue by lazy { decimalRating.setScale(0, RoundingMode.DOWN).intValueExact() }

}
