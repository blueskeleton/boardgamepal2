package com.example.boardgamepal.common.domain.usecases.gamelists;

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class AddGameListUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    name: String
  ) = gameRepository.addGameList(name).flow()
}