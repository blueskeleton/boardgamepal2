package com.example.boardgamepal.common.utils.extension

inline fun <reified T> T.generateList(size: Int): List<T> {
  return (0 until size).toList().map { this }
}

fun <T> Collection<T>.hasAtLeast(size: Int): Boolean {
  return this.size >= size
}

fun <T> Collection<T>?.isNotEmpty(): Boolean {
  return this != null && !this.isEmpty()
}