package com.example.boardgamepal.common.domain.model.game

open class Game(
    val id: String,
    val name: String,
    val numPlayers: PlayersRange,
    val numPlaytime: PlaytimeRange,
    val cover: Image,
    val rating: Rating
) {
  internal class LoadingGame: Game(
    "",
    "",
    PlayersRange.DEFAULT,
    PlaytimeRange.DEFAULT,
    Image.DEFAULT,
    Rating.DEFAULT
  )
}