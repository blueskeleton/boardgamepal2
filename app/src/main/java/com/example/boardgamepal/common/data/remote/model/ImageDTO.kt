package com.example.boardgamepal.common.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageDTO(
  @Json(name = "small") val small: String?,
  @Json(name = "large") val large: String?
)