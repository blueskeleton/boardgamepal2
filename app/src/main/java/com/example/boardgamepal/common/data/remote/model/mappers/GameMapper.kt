package com.example.boardgamepal.common.data.remote.model.mappers

import android.util.Log
import com.example.boardgamepal.common.data.remote.model.GameDTO
import com.example.boardgamepal.common.data.remote.model.ImageDTO
import com.example.boardgamepal.common.domain.model.game.*
import com.example.boardgamepal.common.domain.model.game.Image.Companion.EMPTY_PHOTO

fun GameDTO.toBo(): Game? {
  return try {
    Game(
      id = id ?: throw MappingException("Game id cannot be null"),
      name = name ?: throw MappingException("Game name cannot be null"),
      numPlayers = parseNumPlayers(minPlayers, maxPlayers),
      numPlaytime = parseNumPlaytime(minPlaytime, maxPlaytime),
      cover = parseImage(image),
      rating = parseRating(averageUserRating)
      )
  } catch (e: Exception) {
    Log.e("GameMapper", e.message ?: "Error mapping GameDTO")
    null
  }
}

fun parseNumPlayers(minPlayers: Int?, maxPlayers: Int?): PlayersRange {
  return PlayersRange(minPlayers ?: WRONG_PLAYERS_NUMBER, maxPlayers ?: WRONG_PLAYERS_NUMBER)
}

fun parseNumPlaytime(minPlaytime: Int?, maxPlaytime: Int?): PlaytimeRange {
  return PlaytimeRange(minPlaytime ?: WRONG_PLAYTIME, maxPlaytime ?: WRONG_PLAYTIME)
}

fun parseImage(image: ImageDTO?): Image {
  return Image(
    image?.small ?: EMPTY_PHOTO,
    image?.large ?: EMPTY_PHOTO
  )
}

fun parseRating(averageUserRating: Float?): Rating {
  return Rating(averageUserRating?.let { it * 2 } ?: WRONG_RATING)
}

