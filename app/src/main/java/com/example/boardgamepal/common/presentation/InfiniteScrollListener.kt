package com.example.boardgamepal.common.presentation

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.presentation.listener.GamePaginationListener

class InfiniteScrollListener(
  private val layoutManager: LinearLayoutManager,
  private val pageSize: Int,
  private val listener: GamePaginationListener
) : RecyclerView.OnScrollListener() {

  override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
    super.onScrolled(recyclerView, dx, dy)

    val visibleItemCount = layoutManager.childCount
    val totalItemCount = layoutManager.itemCount
    val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

    if (!listener.isLoading() && !listener.isLastPage()) {
      if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
        && firstVisibleItemPosition >= 0
        && totalItemCount >= pageSize) {
        listener.loadMoreItems()
      }
    }
  }
}