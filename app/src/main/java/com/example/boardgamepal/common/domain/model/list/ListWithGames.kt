package com.example.boardgamepal.common.domain.model.list

import com.example.boardgamepal.common.domain.model.game.Game

data class ListWithGames(
  val list: GameList,
  val games: List<Game>
) {
  val hasExactlyOneGame: Boolean by lazy { games.size == 1 }
  val firstGameImage: String? by lazy { games.firstOrNull()?.cover?.small }
  val secondGameImage: String? by lazy { games.getOrNull(1)?.cover?.small }
  val thirdGameImage: String? by lazy { games.getOrNull(2)?.cover?.small }
  val fourthGameImage: String? by lazy { games.getOrNull(3)?.cover?.small }
}
