package com.example.boardgamepal.common.presentation.widget

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.boardgamepal.R
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.common.utils.extension.getLifecycleOwner
import com.example.boardgamepal.databinding.ViewFourImageGridBinding
import com.example.boardgamepal.mylists.presentation.model.ListWithGamesVO

class FourImageGridView @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

  private var binding: ViewFourImageGridBinding? = null

  var listWithGames: ListWithGamesVO? = null
    set(value) {
      field = value
      binding?.listWithGames = value
      binding?.executePendingBindings()
    }

  init {
    if (isInEditMode) {
      inflate(context, R.layout.view_four_image_grid, this)
    } else {
      binding = ViewFourImageGridBinding.inflate(getLayoutInflater(), this, true)
    }
    binding?.lifecycleOwner = context.getLifecycleOwner()
  }

}