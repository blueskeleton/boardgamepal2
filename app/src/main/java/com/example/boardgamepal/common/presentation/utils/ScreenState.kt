package com.example.boardgamepal.common.presentation.utils

import androidx.annotation.StringRes
import com.example.boardgamepal.R

sealed class ScreenState {
  data class Success(@StringRes val messageRes: Int) : ScreenState()
  data class Error(@StringRes val messageRes: Int) : ScreenState()
  data class Dialog(val type: DialogState) : ScreenState()
  object InvalidateMenu : ScreenState()

  companion object {
    // Success
    val LIST_CREATED = Success(R.string.list_properly_created)
    val LIST_UPDATED = Success(R.string.list_properly_updated)
    val LIST_DELETED = Success(R.string.list_properly_deleted)
    val GAME_ADDED_TO_COLLECTION = Success(R.string.game_added_to_collection)
    val GAME_ADDED_TO_WISHLIST = Success(R.string.game_added_to_wishlist)
    val GAME_DELETED_FROM_COLLECTION = Success(R.string.game_deleted_from_collection)
    val GAME_DELETED_FROM_WISHLIST = Success(R.string.game_deleted_from_wishlist)
    val GAME_ADDED_TO_LISTS = Success(R.string.game_added_to_lists)
    val GAME_DELETED_FROM_LISTS = Success(R.string.game_deleted_from_lists)

    // Error
    val ADD_GAME_ERROR = Error(R.string.add_game_error)
    val DELETE_GAME_ERROR = Error(R.string.delete_game_error)
    val LIST_EMPTY = Error(R.string.list_cannot_be_empty)
    val LIST_TWICE = Error(R.string.list_already_exists)
    val ADD_TO_LISTS_ERROR = Error(R.string.add_to_lists_error)
    val DELETE_FROM_LISTS_ERROR = Error(R.string.delete_from_lists)
    val UPDATE_LIST_ERROR = Error(R.string.update_list_error)
    val DELETE_LIST_ERROR = Error(R.string.delete_list_error)
  }
}

enum class DialogState {
  SHOW_LISTS_TO_ADD,
  SHOW_LISTS_TO_REMOVE
}