package com.example.boardgamepal.common.domain.usecases.collection

import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class AddGameToCollectionUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    detail: GameDetail
  ) = gameRepository.addGameToCollection(detail).flow()
}