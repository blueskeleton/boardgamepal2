package com.example.boardgamepal.common.data.repository

import com.example.boardgamepal.common.data.local.datasource.GameLocalDataSource
import com.example.boardgamepal.common.data.remote.datasource.GameRemoteDataSource
import com.example.boardgamepal.common.data.remote.model.mappers.toGame
import com.example.boardgamepal.common.data.repository.util.RepositoryResponse
import com.example.boardgamepal.common.data.repository.util.localResponse
import com.example.boardgamepal.common.data.repository.util.remoteResponse
import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames
import com.example.boardgamepal.common.domain.repository.GameRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GameRepositoryImpl @Inject constructor(
  private val remote: GameRemoteDataSource,
  private val local: GameLocalDataSource
) : GameRepository {

  //region Remote - Explore
  override suspend fun getTopGamesPagination(pageNumber: Int): RepositoryResponse<GamePagination> {
    return remoteResponse { remote.getTopGames(pageNumber) }
  }

  override suspend fun getGamesSearch(name: String, pageNumber: Int): RepositoryResponse<GamePagination> {
    return remoteResponse { remote.getGamesSearch(name, pageNumber) }
  }
  //endregion

  //region Remote - Detail
  override suspend fun getGameDetail(gameId: String): RepositoryResponse<GameDetail?> {
    return remoteResponse { remote.getGameDetail(gameId) }
  }
  //endregion

  //region Local - Collection
  override suspend fun getAllCollection(): Flow<List<Game>> {
    return withContext(Dispatchers.IO) {
      local.getAllCollection()
    }
  }

  override suspend fun addGameToCollection(detail: GameDetail): RepositoryResponse<Long> {
    return localResponse { local.addGameToCollection(detail.toGame()) }
  }

  override suspend fun deleteGameFromCollection(detail: GameDetail): RepositoryResponse<Int> {
    return localResponse { local.deleteGameFromCollection(detail.toGame()) }
  }

  override suspend fun isGameInCollection(gameId: String): Flow<Boolean> {
    return withContext(Dispatchers.IO) {
      local.isGameInCollection(gameId).map { it != null }
    }
  }
  //endregion

  //region Local - WishList
  override suspend fun getAllWishList(): Flow<List<Game>> {
    return withContext(Dispatchers.IO) {
      local.getAllWishList()
    }
  }

  override suspend fun addGameToWishList(detail: GameDetail): RepositoryResponse<Long> {
    return localResponse { local.addGameToWishList(detail.toGame()) }
  }

  override suspend fun deleteGameFromWishList(detail: GameDetail): RepositoryResponse<Int> {
    return localResponse { local.deleteGameFromWishList(detail.toGame()) }
  }

  override suspend fun isGameInWishList(gameId: String): Flow<Boolean> {
    return withContext(Dispatchers.IO) {
      local.isGameInWishList(gameId).map { it != null }
    }
  }
  //endregion

  //region Local - Lists
  override suspend fun getAllLists(): Flow<List<ListWithGames>> {
    return withContext(Dispatchers.IO) {
      local.getAllLists()
    }
  }

  override suspend fun getListsWithoutGame(gameId: String): Flow<List<GameList>> {
    return withContext(Dispatchers.IO) {
      local.getListsWithoutGame(gameId)
    }
  }

  override suspend fun getListsWithThisGame(gameId: String): Flow<List<GameList>> {
    return withContext(Dispatchers.IO) {
      local.getListsWithThisGame(gameId)
    }
  }

  override suspend fun addGameList(name: String): RepositoryResponse<Long> {
    return localResponse { local.addGameList(name) }
  }

  override suspend fun updateGameList(list: GameList): RepositoryResponse<Int> {
    return localResponse { local.updateGameList(list) }
  }

  override suspend fun deleteGameList(list: GameList): RepositoryResponse<Int> {
    return localResponse { local.deleteGameList(list) }
  }

  override suspend fun addGameToLists(detail: GameDetail, lists: List<GameList>): RepositoryResponse<List<Long>> {
    return localResponse { local.addGameToLists(detail.toGame(), lists) }
  }

  override suspend fun deleteGameFromLists(detail: GameDetail, lists: List<GameList>): RepositoryResponse<Int> {
    return localResponse { local.deleteGameFromLists(detail.toGame(), lists) }
  }
  //endregion
}