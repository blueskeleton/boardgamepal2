package com.example.boardgamepal.common.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GameDetailDTO(
  @Json(name = "id") val id: String?,
  @Json(name = "name") val name: String?,
  @Json(name = "pfGame") val extraDetails: ExtraGameDetailDTO?,
  @Json(name = "description") val description: String?,
  @Json(name = "min_players") val minPlayers: Int?,
  @Json(name = "max_players") val maxPlayers: Int?,
  @Json(name = "min_playtime") val minPlaytime: Int?,
  @Json(name = "max_playtime") val maxPlaytime: Int?,
  @Json(name = "images") val image: ImageDTO?,
  @Json(name = "rank") val rank: Int?,
  @Json(name = "average_user_rating") val averageUserRating: Float?,
  @Json(name = "year_published") val yearPublished: Int?,
  @Json(name = "designers") val designers: List<DesignerDTO>?,
  @Json(name = "artists") val artists: List<String>?,
  @Json(name = "mechanics") val mechanics: List<MechanicDTO>?,
  @Json(name = "categories") val categories: List<CategoryDTO>?,
)
