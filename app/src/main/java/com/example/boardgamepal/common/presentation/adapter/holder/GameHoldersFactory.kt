package com.example.boardgamepal.common.presentation.adapter.holder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.R
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.databinding.RowGameItemBinding

class GameHoldersFactory {

  companion object {
    const val LOADING_PRODUCT = 1
    const val LIST_PRODUCT = 2
  }

  internal fun getItemViewType(game: Game): Int {
    return when (game) {
      is Game.LoadingGame -> LOADING_PRODUCT
      else -> LIST_PRODUCT
    }
  }

  internal fun onCreateViewHolder(parameters: GameListHoldersFactoryParameters): RecyclerView.ViewHolder {
    val layoutInflater = parameters.parent.getLayoutInflater()

    return when (parameters.viewType) {
      LIST_PRODUCT -> {
        GameViewHolder(
          RowGameItemBinding.inflate(layoutInflater, parameters.parent, false),
          parameters.commonViewModel
        )
      }
      else -> {
        LoadingViewHolder(layoutInflater.inflate(R.layout.row_game_loading, parameters.parent, false))
      }
    }
  }

  internal fun onBindViewHolder(
    holder: RecyclerView.ViewHolder,
    game: Game
  ) {
    (holder as? GameViewHolder)?.bind(game)
  }

  //region ViewHolder
  inner class GameViewHolder(
    private val binding: RowGameItemBinding,
    private val viewModel: CommonBaseViewModel
    ) : RecyclerView.ViewHolder(binding.root) {
    fun bind(game: Game) {
      binding.game = game
      binding.viewModel = viewModel
    }
  }

  inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
  //endregion

  data class GameListHoldersFactoryParameters(
    val parent: ViewGroup,
    val viewType: Int,
    val commonViewModel: CommonBaseViewModel
  )

}