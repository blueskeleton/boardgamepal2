package com.example.boardgamepal.common.data.repository.util

import com.example.boardgamepal.common.data.model.AsyncError

sealed class AsyncResult<out T>(open val data: T?) {
  data class Success<out T>(override val data: T?) : AsyncResult<T>(data)
  data class Error<out T>(val error: AsyncError, override val data: T?) : AsyncResult<T>(data)
  data class Loading<out T>(override val data: T?) : AsyncResult<T>(data)

  fun isLoading() = this is Loading
  fun isSuccess() = this is Success && data != null
  fun isError() = this is Error
  fun isConnectionLost() = this is Error && error is AsyncError.ConnectionError
}

fun <T> AsyncResult<T>?.isSuccess() = this is AsyncResult.Success && data != null

fun <T> AsyncResult<T>?.getSuccessData(): T? {
  return this.takeIf { it is AsyncResult.Success }?.data
}