package com.example.boardgamepal.common.data.remote.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CardsDTO(
  @Json(name = "card_quantity") val cardQuantity: Int?,
  @Json(name = "height") val height: String?,
  @Json(name = "width") val width: String?
)