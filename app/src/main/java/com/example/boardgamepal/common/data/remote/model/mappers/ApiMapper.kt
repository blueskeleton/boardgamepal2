package com.example.boardgamepal.common.data.remote.model.mappers

interface ApiMapper<E, D> {
  fun mapToDomain(apiEntity: E): D
}