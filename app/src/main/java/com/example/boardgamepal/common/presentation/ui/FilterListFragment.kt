package com.example.boardgamepal.common.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.presentation.viewmodel.FilterListViewModel
import com.example.boardgamepal.common.utils.extension.setFilterListForResult
import com.example.boardgamepal.databinding.FragmentFilterListBinding
import com.example.boardgamepal.mycollection.presentation.viewmodel.MyCollectionFiltersViewModel
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsFiltersViewModel
import com.example.boardgamepal.wishlist.presentation.viewmodel.WishListFiltersViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.math.RoundingMode

@AndroidEntryPoint
class FilterListFragment : CommonBaseFragment() {

  companion object {
    const val COLLECTION = "collection"
    const val WISHLIST = "wishlist"
    const val LIST = "list"
  }

  private lateinit var binding: FragmentFilterListBinding

  private val args: FilterListFragmentArgs by navArgs()
  private val viewModel: FilterListViewModel by lazy {
    when (args.origin) {
      COLLECTION -> ViewModelProvider(requireActivity())[MyCollectionFiltersViewModel::class.java]
      WISHLIST -> ViewModelProvider(requireActivity())[WishListFiltersViewModel::class.java]
      LIST -> ViewModelProvider(requireActivity())[MyListsFiltersViewModel::class.java]
      else -> ViewModelProvider(requireActivity())[FilterListViewModel::class.java]
    }
  }

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.filter_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    binding = FragmentFilterListBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      filterViewModel = viewModel
    }

    return binding.root
  }

  override fun initializeView() {
    viewModel.saveOldValues()
    binding.filterListSliderPlayers.setLabelFormatter { label ->
      if (label == 12f) {
        "${label.toInt()}+"
      } else {
        label.toInt().toString()
      }
    }
    binding.filterListSliderPlaytime.setLabelFormatter { label ->
      when (label) {
        1f -> "0 min"
        2f -> "5 min"
        3f -> "10 min"
        4f -> "15 min"
        5f -> "30 min"
        6f -> "45 min"
        7f -> "60 min"
        8f -> "1.5h"
        9f -> "2h"
        10f -> "2.5h"
        11f -> "3h"
        12f -> "3.5h"
        13f -> "4h"
        14f -> "5h"
        15f -> "6+h"
        else -> label.toString()
      }
    }
    binding.filterListSliderRating.setLabelFormatter { label ->
      label.toBigDecimal().setScale(1, RoundingMode.HALF_EVEN).toString()
    }
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_filter -> {
        setFilterListForResult(true)
        findNavController().navigateUp()
        true
      }
      else -> {
        viewModel.resetOldValues()
        setFilterListForResult(false)
        false
      }
    }
  }
}