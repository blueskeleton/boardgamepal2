package com.example.boardgamepal.common.data.local.di

import android.app.Application
import com.example.boardgamepal.common.data.local.AppDatabase
import com.example.boardgamepal.common.data.local.datasource.CollectionDao
import com.example.boardgamepal.common.data.local.datasource.GameListDao
import com.example.boardgamepal.common.data.local.datasource.GameLocalDataSource
import com.example.boardgamepal.common.data.local.datasource.GameLocalDataSourceImpl
import com.example.boardgamepal.common.data.local.datasource.WishListDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

  @Provides
  @Singleton
  fun appRoomDatabaseProvider(context: Application): AppDatabase {
    return AppDatabase.getDatabase(context)
  }

  @Provides
  fun collectionDaoProvider(database: AppDatabase): CollectionDao {
    return database.collectionDao()
  }

  @Provides
  fun wishListDaoProvider(database: AppDatabase): WishListDao {
    return database.wishListDao()
  }

  @Provides
  fun gameListDaoProvider(database: AppDatabase): GameListDao {
    return database.gameListDao()
  }

  @Provides
  fun gameLocalDataSourceProvider(
    collectionDao: CollectionDao,
    wishListDao: WishListDao,
    gameListDao: GameListDao
  ): GameLocalDataSource {
    return GameLocalDataSourceImpl(collectionDao, wishListDao, gameListDao)
  }

}