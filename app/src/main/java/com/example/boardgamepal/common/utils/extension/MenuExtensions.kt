package com.example.boardgamepal.common.utils.extension

import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.boardgamepal.R

fun MenuItem?.setFavoriteIcon(isFavorite: Boolean) {
  this?.apply {
    setIcon(if (isFavorite) R.drawable.ic_favorite else R.drawable.ic_not_favorite)
  }
}

fun Fragment.invalidateMenu() {
  this.activity?.invalidateOptionsMenu()
}