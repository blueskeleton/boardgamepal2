package com.example.boardgamepal.common.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.boardgamepal.common.data.local.datasource.CollectionDao
import com.example.boardgamepal.common.data.local.datasource.GameListDao
import com.example.boardgamepal.common.data.local.datasource.WishListDao
import com.example.boardgamepal.common.data.local.model.CollectionGameDbo
import com.example.boardgamepal.common.data.local.model.GameDbo
import com.example.boardgamepal.common.data.local.model.GameListAndGameCrossRefDbo
import com.example.boardgamepal.common.data.local.model.GameListDbo
import com.example.boardgamepal.common.data.local.model.WishListGameDbo

@Database(
  entities = [
    CollectionGameDbo::class,
    WishListGameDbo::class,
    GameDbo::class,
    GameListDbo::class,
    GameListAndGameCrossRefDbo::class],
  version = 1,
  exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

  abstract fun collectionDao(): CollectionDao
  abstract fun wishListDao(): WishListDao
  abstract fun gameListDao(): GameListDao

  companion object {
    private const val DATABASE_NAME = "board_games_database.db"
    @Volatile
    private var INSTANCE: AppDatabase? = null

    fun getDatabase(context: Context): AppDatabase {
      return INSTANCE ?: synchronized(this) {
        val instance = Room.databaseBuilder(
          context.applicationContext,
          AppDatabase::class.java,
          DATABASE_NAME
        )
          .createFromAsset("database/board_games_database.db")
          .fallbackToDestructiveMigration()
          .build()
        INSTANCE = instance

        instance
      }
    }
  }
}