package com.example.boardgamepal.common.presentation.binding

import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.navigation.NavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.R
import com.example.boardgamepal.common.utils.extension.isNotTrue
import com.example.boardgamepal.common.utils.extension.isTrue
import com.google.android.material.bottomnavigation.BottomNavigationView


object CommonBinding {

  @BindingAdapter("bind:setupWithNavController")
  @JvmStatic
  fun setupWithNavController(view: BottomNavigationView, navController: NavController?) {
    navController?.let {
      view.setupWithNavController(it)
      it.addOnDestinationChangedListener { _, _, arguments ->
        view.isVisible = arguments?.getBoolean("HIDE_BOTTOM_NAV", false).isNotTrue()
      }
    }
  }

  @BindingAdapter("bind:setupWithNavController")
  @JvmStatic
  fun setupWithNavController(view: Toolbar, navController: NavController?) {
    navController?.let {
      it.addOnDestinationChangedListener { _, _, arguments ->
        if (arguments?.getBoolean("SHOW_BACK_AS_CLOSE", false).isTrue()) {
          view.navigationIcon = AppCompatResources.getDrawable(view.context, R.drawable.ic_close)
        }
      }
    }
  }

  @BindingAdapter("bind:showIf")
  @JvmStatic
  fun showIf(view: View, condition: Boolean?) {
    view.isVisible = condition.isTrue()
  }

  @BindingAdapter("bind:showIfInvisible")
  @JvmStatic
  fun showIfInvisible(view: View, condition: Boolean?) {
    view.visibility = if (condition.isTrue()) View.VISIBLE else View.INVISIBLE
  }

  @BindingAdapter("bind:itemDecoration")
  @JvmStatic
  fun itemDecoration(recyclerView: RecyclerView, itemDecoration: RecyclerView.ItemDecoration?) {
    itemDecoration?.let { recyclerView.addItemDecoration(it) }
  }

  @BindingAdapter("bind:htmlText")
  @JvmStatic
  fun htmlText(view: TextView, text: String?) {
    text?.let {
      view.text = Html.fromHtml(it)
    }
  }

}