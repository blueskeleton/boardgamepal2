package com.example.boardgamepal.common.data.remote.error

import com.example.boardgamepal.common.data.model.AsyncError
import com.example.boardgamepal.common.data.model.CoreException
import com.squareup.moshi.JsonDataException
import retrofit2.HttpException
import java.net.UnknownHostException
import java.util.concurrent.CancellationException

object RemoteErrorManagement {

  /**
   * Calls the specified function [block] and returns its result. Catch any Exception and converts it to a CoreException.
   */
  inline fun <T> wrap(block: () -> T): T {
    return try {
      block()
    } catch (cancellation: CancellationException) {
      // Do not intercept and wrap CancellationException, this is needed to manage the different coroutines
      // to know if those have been cancelled
      throw cancellation
    } catch (e: Throwable) {
      throw CoreException(processError(e))
    }
  }

  fun processError(throwable: Throwable): AsyncError {
    return when (throwable) {
      is HttpException -> processRetrofitError(throwable)
      is UnknownHostException -> AsyncError.ConnectionError(throwable.message ?: "Error de conexion")
      is JsonDataException -> AsyncError.DataParseError(throwable.message ?: "Error de parseo")
      else -> AsyncError.UnknownError(throwable.message ?: "Error desconocido", throwable)
    }
  }

  private fun processRetrofitError(httpException: HttpException): AsyncError {
    val errorCode = httpException.code()
    val url = httpException.response()?.raw()?.request()?.url().toString()
    return AsyncError.ServerError(errorCode, url)
  }
}