package com.example.boardgamepal.common.data.local.model.mappers

import com.example.boardgamepal.common.data.local.model.GameListAndGameCrossRefDbo
import com.example.boardgamepal.common.data.local.model.GameListDbo
import com.example.boardgamepal.common.data.local.model.ListWithGamesDbo
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames

fun GameListDbo.toBo(): GameList {
  return GameList(listId, name)
}

fun GameList.toDbo(): GameListDbo {
  return GameListDbo(id, name)
}

fun List<GameList>.toCrossRefDbo(gameId: String): List<GameListAndGameCrossRefDbo> {
  return this.map { GameListAndGameCrossRefDbo(it.id, gameId) }
}

fun ListWithGamesDbo.toBo(): ListWithGames {
  return ListWithGames(list.toBo(), games.map { it.toBo() })
}