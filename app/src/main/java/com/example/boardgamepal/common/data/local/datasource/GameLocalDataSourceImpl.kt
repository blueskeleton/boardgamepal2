package com.example.boardgamepal.common.data.local.datasource

import com.example.boardgamepal.common.data.local.model.GameListDbo
import com.example.boardgamepal.common.data.local.model.mappers.*
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GameLocalDataSourceImpl @Inject constructor(
  private val collectionDao: CollectionDao,
  private val wishListDao: WishListDao,
  private val gameListDao: GameListDao
) : GameLocalDataSource {
  override suspend fun getAllCollection(): Flow<List<Game>> {
    return collectionDao.getAllCollectionGames().map { list ->
      list.map { it.toBo() }
    }
  }

  override suspend fun addGameToCollection(game: Game): Long {
    return collectionDao.save(game.toCollectionDbo())
  }

  override suspend fun deleteGameFromCollection(game: Game): Int {
    return collectionDao.deleteGame(game.toCollectionDbo())
  }

  override suspend fun isGameInCollection(gameId: String): Flow<String?> {
    return collectionDao.isGameInCollection(gameId)
  }

  override suspend fun getAllWishList(): Flow<List<Game>> {
    return wishListDao.getAllWishListGames().map { list ->
      list.map { it.toBo() }
    }
  }

  override suspend fun addGameToWishList(game: Game): Long {
    return wishListDao.save(game.toWishListDbo())
  }

  override suspend fun deleteGameFromWishList(game: Game): Int {
    return wishListDao.deleteGame(game.toWishListDbo())
  }

  override suspend fun isGameInWishList(gameId: String): Flow<String?> {
    return wishListDao.isGameInWishList(gameId)
  }

  override suspend fun getAllLists(): Flow<List<ListWithGames>> {
    return gameListDao.getAllLists().map { gameLists ->
      gameLists.map { it.toBo() }
    }
  }

  override suspend fun getListsWithoutGame(gameId: String): Flow<List<GameList>> {
    return gameListDao.getListsWithoutGame(gameId).map { gameLists ->
      gameLists.map { it.toBo() }
    }
  }

  override suspend fun getListsWithThisGame(gameId: String): Flow<List<GameList>> {
    return gameListDao.getListsWithThisGame(gameId).map { gameLists ->
      gameLists.map { it.toBo() }
    }
  }

  override suspend fun addGameList(name: String): Long {
    return gameListDao.createList(GameListDbo(name = name))
  }

  override suspend fun updateGameList(list: GameList): Int {
    return gameListDao.updateList(list.toDbo())
  }

  override suspend fun deleteGameList(list: GameList): Int {
    return gameListDao.deleteList(list.toDbo())
  }

  override suspend fun addGameToLists(game: Game, lists: List<GameList>): List<Long> {
    return gameListDao.addGameToLists(game.toDbo(), lists.toCrossRefDbo(game.id))
  }

  override suspend fun deleteGameFromLists(game: Game, lists: List<GameList>): Int {
    return gameListDao.deleteGameFromLists(game.toDbo(), lists.toCrossRefDbo(game.id))
  }
}