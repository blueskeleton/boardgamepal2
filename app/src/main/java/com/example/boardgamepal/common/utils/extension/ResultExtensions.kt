package com.example.boardgamepal.common.utils.extension

import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.presentation.utils.ScreenState
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel

fun <T> AsyncResult<T>.sendEvents(
  viewModel: CommonBaseViewModel,
  success: ScreenState.Success?,
  error: ScreenState.Error?
) {
  when (this) {
    is AsyncResult.Success -> success?.let { viewModel.sendSuccessEvent(it) }
    is AsyncResult.Error -> error?.let { viewModel.sendErrorEvent(it) }
    is AsyncResult.Loading -> {/* no-op */}
  }
}