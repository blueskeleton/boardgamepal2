package com.example.boardgamepal.common.utils.extension

fun String?.isNotEmpty() = !this.isNullOrEmpty()