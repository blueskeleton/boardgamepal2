package com.example.boardgamepal.common.domain.usecases.gamelists;

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class GetAllListsUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke() = gameRepository.getAllLists()
}