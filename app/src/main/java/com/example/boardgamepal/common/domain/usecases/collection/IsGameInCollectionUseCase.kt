package com.example.boardgamepal.common.domain.usecases.collection

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class IsGameInCollectionUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    gameId: String
  ) = gameRepository.isGameInCollection(gameId)
}