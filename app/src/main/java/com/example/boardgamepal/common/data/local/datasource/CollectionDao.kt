package com.example.boardgamepal.common.data.local.datasource

import androidx.room.Dao
import androidx.room.Query
import com.example.boardgamepal.common.data.local.model.CollectionGameDbo
import kotlinx.coroutines.flow.Flow

@Dao
abstract class CollectionDao : BaseDao<CollectionGameDbo>() {

  @Query("SELECT * FROM collection")
  abstract fun getAllCollectionGames(): Flow<List<CollectionGameDbo>>

  @Query("SELECT id FROM collection WHERE id = :id LIMIT 1")
  abstract fun isGameInCollection(id: String): Flow<String?>

  suspend fun save(game: CollectionGameDbo): Long {
    return insert(game)
  }

  suspend fun save(games: List<CollectionGameDbo>) {
    insert(games)
  }

  suspend fun deleteGame(game: CollectionGameDbo): Int {
    return delete(game)
  }

}