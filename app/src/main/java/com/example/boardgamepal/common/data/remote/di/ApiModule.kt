package com.example.boardgamepal.common.data.remote.di

import com.example.boardgamepal.common.data.remote.datasource.GameRemoteDataSource
import com.example.boardgamepal.common.data.remote.datasource.GameRemoteDataSourceImpl
import com.example.boardgamepal.common.data.remote.datasource.GameWs
import com.example.boardgamepal.common.data.remote.utils.ApiConstants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

  @Provides
  @Singleton
  fun gameWsProvider(api: Retrofit): GameWs {
    return api.create(GameWs::class.java)
  }

  @Provides
  fun gameRemoteDataSourceProvider(gameWs: GameWs): GameRemoteDataSource {
    return GameRemoteDataSourceImpl(gameWs)
  }

  @Provides
  fun retrofitProvider(): Retrofit {
    return Retrofit.Builder()
      .baseUrl(ApiConstants.BASE_ENDPOINT)
      .addConverterFactory(MoshiConverterFactory.create())
      .build()
  }

}