package com.example.boardgamepal.common.utils.extension

import androidx.annotation.StringRes
import com.leinardi.android.speeddial.SpeedDialActionItem

fun SpeedDialActionItem.Builder.setCustomLabel(
  @StringRes stringRes: Int
): SpeedDialActionItem.Builder {
  return this.setLabel(stringRes)
}