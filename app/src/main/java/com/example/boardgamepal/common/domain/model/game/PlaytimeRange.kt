package com.example.boardgamepal.common.domain.model.game

const val WRONG_PLAYTIME = -1

data class PlaytimeRange(
  val minPlaytime: Int,
  val maxPlaytime: Int
) {

  companion object {
    val DEFAULT = PlaytimeRange(WRONG_PLAYTIME, WRONG_PLAYTIME)
  }

  val isValid by lazy { minPlaytime != WRONG_PLAYTIME && maxPlaytime != WRONG_PLAYTIME }
  val timeText by lazy { toText() }
  val fullTimeText by lazy { toFullText() }

  fun contains(playtimeNumber: Int) = playtimeNumber in minPlaytime..maxPlaytime

  private fun toText(): String? {
    return when {
      !isValid -> null
      minPlaytime == maxPlaytime -> "$minPlaytime min"
      else -> "$minPlaytime - $maxPlaytime min"
    }
  }

  private fun toFullText(): String? {
    return when {
      !isValid -> null
      minPlaytime == maxPlaytime -> "$minPlaytime minutos"
      else -> "$minPlaytime - $maxPlaytime minutos"
    }
  }
}