package com.example.boardgamepal.common.domain.repository

import com.example.boardgamepal.common.data.repository.util.RepositoryResponse
import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames
import kotlinx.coroutines.flow.Flow

interface GameRepository {
  // Api
  suspend fun getTopGamesPagination(pageNumber: Int): RepositoryResponse<GamePagination>
  suspend fun getGamesSearch(name: String, pageNumber: Int): RepositoryResponse<GamePagination>
  suspend fun getGameDetail(gameId: String): RepositoryResponse<GameDetail?>

  // Local - Collection
  suspend fun getAllCollection(): Flow<List<Game>>
  suspend fun addGameToCollection(detail: GameDetail): RepositoryResponse<Long>
  suspend fun deleteGameFromCollection(detail: GameDetail): RepositoryResponse<Int>
  suspend fun isGameInCollection(gameId: String): Flow<Boolean>

  // Local - WishList
  suspend fun getAllWishList(): Flow<List<Game>>
  suspend fun addGameToWishList(detail: GameDetail): RepositoryResponse<Long>
  suspend fun deleteGameFromWishList(detail: GameDetail): RepositoryResponse<Int>
  suspend fun isGameInWishList(gameId: String): Flow<Boolean>

  // Local - GameLists
  suspend fun getAllLists(): Flow<List<ListWithGames>>
  suspend fun getListsWithoutGame(gameId: String): Flow<List<GameList>>
  suspend fun getListsWithThisGame(gameId: String): Flow<List<GameList>>
  suspend fun addGameList(name: String): RepositoryResponse<Long>
  suspend fun updateGameList(list: GameList): RepositoryResponse<Int>
  suspend fun deleteGameList(list: GameList): RepositoryResponse<Int>
  suspend fun addGameToLists(detail: GameDetail, lists: List<GameList>): RepositoryResponse<List<Long>>
  suspend fun deleteGameFromLists(detail: GameDetail, lists: List<GameList>): RepositoryResponse<Int>
}