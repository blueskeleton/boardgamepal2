package com.example.boardgamepal.common.domain.usecases.collection

import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class DeleteGameFromCollectionUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    detail: GameDetail
  ) = gameRepository.deleteGameFromCollection(detail).flow()
}