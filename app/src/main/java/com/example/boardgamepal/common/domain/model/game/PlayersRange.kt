package com.example.boardgamepal.common.domain.model.game

const val WRONG_PLAYERS_NUMBER = -1

data class PlayersRange(
  val minPlayers: Int,
  val maxPlayers: Int
) {

  companion object {
    val DEFAULT = PlayersRange(WRONG_PLAYERS_NUMBER, WRONG_PLAYERS_NUMBER)
  }

  val isValid by lazy { minPlayers != WRONG_PLAYERS_NUMBER && maxPlayers != WRONG_PLAYERS_NUMBER }
  val playersText by lazy { toText() }

  fun contains(playersNumber: Int) = playersNumber in minPlayers..maxPlayers

  private fun toText(): String? {
    return when {
      !isValid -> null
      minPlayers == maxPlayers -> "$minPlayers"
      else -> "$minPlayers - $maxPlayers"
    }
  }
}
