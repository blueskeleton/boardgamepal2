package com.example.boardgamepal.common.presentation.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.R
import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.data.repository.util.getSuccessData
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.presentation.adapter.GameDetailAdapter
import com.example.boardgamepal.common.presentation.adapter.SimpleTextAdapter
import com.example.boardgamepal.common.presentation.adapter.holder.GameDetailHoldersFactory
import com.example.boardgamepal.common.presentation.listener.SpeedDialListener
import com.example.boardgamepal.common.presentation.utils.DetailRow
import com.example.boardgamepal.common.presentation.utils.SpeedDialManager.getAddToCollectionAction
import com.example.boardgamepal.common.presentation.utils.SpeedDialManager.getAddToListsAction
import com.example.boardgamepal.common.presentation.utils.SpeedDialManager.getDeleteFromCollectionAction
import com.example.boardgamepal.common.presentation.utils.SpeedDialManager.getDeleteFromListsAction
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.presentation.viewmodel.GameDetailViewModel
import com.example.boardgamepal.common.utils.extension.isNotEmpty
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.common.utils.extension.setMainDetailImage
import com.example.boardgamepal.explore.presentation.ui.ExploreFragmentDirections
import com.example.boardgamepal.explore.presentation.ui.SearchGamesFragmentDirections
import com.example.boardgamepal.explore.presentation.viewmodel.ExploreViewModel
import com.example.boardgamepal.explore.presentation.viewmodel.SearchGamesViewModel
import com.example.boardgamepal.mycollection.presentation.ui.MyCollectionFragmentDirections
import com.example.boardgamepal.mycollection.presentation.viewmodel.MyCollectionViewModel
import com.example.boardgamepal.mylists.presentation.ui.ListDetailFragmentDirections
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsViewModel
import com.example.boardgamepal.wishlist.presentation.ui.WishListFragmentDirections
import com.example.boardgamepal.wishlist.presentation.viewmodel.WishListViewModel
import com.leinardi.android.speeddial.SpeedDialView

object GameDetailBinding {

  @BindingAdapter(value = ["bind:goToGameDetail", "bind:commonBaseViewModel"])
  @JvmStatic
  fun goToGameDetail(
    view: View,
    game: Game?,
    viewModel: CommonBaseViewModel?
  ) {
    if (game != null && viewModel != null) {
      view.setOnClickListener {
        val navDirections = when (viewModel) {
          is ExploreViewModel -> ExploreFragmentDirections.actionGoToGameDetail(game.id, game.name)
          is SearchGamesViewModel -> SearchGamesFragmentDirections.actionGoToGameDetail(game.id, game.name)
          is MyCollectionViewModel -> MyCollectionFragmentDirections.actionGoToGameDetail(game.id, game.name)
          is WishListViewModel -> WishListFragmentDirections.actionGoToGameDetail(game.id, game.name)
          is MyListsViewModel -> ListDetailFragmentDirections.actionGoToGameDetail(game.id, game.name)
          else -> null
        }

        navDirections?.let { viewModel.navigate(it) }
      }
    }
  }

  @BindingAdapter(value = [
    "bind:gameDetail",
    "bind:gameDetailAdapter",
    "bind:gameDetailViewModel"
  ])
  @JvmStatic
  fun gameDetailAdapter(
    view: RecyclerView,
    detail: AsyncResult<GameDetail?>?,
    rows: List<DetailRow>?,
    viewModel: GameDetailViewModel?
  ) {
    if (view.adapter == null && viewModel != null) {
      detail.getSuccessData()?.let {
        view.adapter = GameDetailAdapter(it, viewModel, GameDetailHoldersFactory())
      }
    }

    (view.adapter as? GameDetailAdapter)?.submitList(rows)
  }

  @BindingAdapter("bind:loadMainDetailImage")
  @JvmStatic
  fun loadMainDetailImage(
    view: ImageView,
    game: GameDetail?
  ) {
    view.setMainDetailImage(game)
  }

  @BindingAdapter("bind:simpleTextAdapter")
  @JvmStatic
  fun setSimpleTextAdapter(
    view: RecyclerView,
    list: List<String>?
  ) {
    if (view.adapter == null && list != null) {
      view.adapter = SimpleTextAdapter()
      view.setHasFixedSize(true)
    }

    (view.adapter as? SimpleTextAdapter)?.submitList(list)
  }

  @BindingAdapter(value = [
    "bind:speedDialListener",
    "bind:isGameInCollection",
    "bind:listsWithGame",
    "bind:listsWithoutGame"
  ])
  @JvmStatic
  fun setSpeedDial(
    view: SpeedDialView,
    listener: SpeedDialListener?,
    isGameInCollection: Boolean?,
    listsWithGame: List<GameList>?,
    listsWithoutGame: List<GameList>?
  ) {
    if (listener != null) {
      view.setOnActionSelectedListener { actionItem ->
        when (actionItem.id) {
          R.id.fab_add_to_collection -> {
            listener.addGameToCollection()
            view.close()
            return@setOnActionSelectedListener true
          }
          R.id.fab_delete_from_collection -> {
            listener.deleteGameFromCollection()
            view.close()
            return@setOnActionSelectedListener true
          }
          R.id.fab_add_to_lists -> {
            listener.showListsToAdd()
            view.close()
            return@setOnActionSelectedListener true
          }
          R.id.fab_delete_from_lists -> {
            listener.showListsToDelete()
            view.close()
            return@setOnActionSelectedListener true
          }
        }
        false
      }
    }

    view.apply {
      clearActionItems()

      if (isGameInCollection.isTrue()) {
        addActionItem(getDeleteFromCollectionAction())
      } else {
        addActionItem(getAddToCollectionAction())
      }

      if (listsWithoutGame.isNotEmpty()) {
        addActionItem(getAddToListsAction())
      }

      if (listsWithGame.isNotEmpty()) {
        addActionItem(getDeleteFromListsAction())
      }
    }
  }

}