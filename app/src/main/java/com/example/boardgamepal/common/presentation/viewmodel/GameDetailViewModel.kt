package com.example.boardgamepal.common.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.data.repository.util.getSuccessData
import com.example.boardgamepal.common.data.repository.util.isSuccess
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.usecases.api.GetGameDetailUseCase
import com.example.boardgamepal.common.domain.usecases.collection.AddGameToCollectionUseCase
import com.example.boardgamepal.common.domain.usecases.collection.DeleteGameFromCollectionUseCase
import com.example.boardgamepal.common.domain.usecases.collection.IsGameInCollectionUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.AddGameToListsUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.DeleteGameFromListsUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.GetListsWithThisGameUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.GetListsWithoutGameUseCase
import com.example.boardgamepal.common.domain.usecases.wishlist.AddGameToWishListUseCase
import com.example.boardgamepal.common.domain.usecases.wishlist.DeleteGameFromWishListUseCase
import com.example.boardgamepal.common.domain.usecases.wishlist.IsGameInWishListUseCase
import com.example.boardgamepal.common.presentation.listener.SpeedDialListener
import com.example.boardgamepal.common.presentation.utils.DetailRow
import com.example.boardgamepal.common.presentation.utils.DetailRowManager
import com.example.boardgamepal.common.presentation.utils.DialogState
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.ADD_GAME_ERROR
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.ADD_TO_LISTS_ERROR
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.DELETE_FROM_LISTS_ERROR
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.DELETE_GAME_ERROR
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_ADDED_TO_COLLECTION
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_ADDED_TO_LISTS
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_ADDED_TO_WISHLIST
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_DELETED_FROM_COLLECTION
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_DELETED_FROM_LISTS
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.GAME_DELETED_FROM_WISHLIST
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.common.utils.extension.sendEvents
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameDetailViewModel @Inject constructor(
  private val getGameDetailUseCase: GetGameDetailUseCase,
  private val addGameToCollectionUseCase: AddGameToCollectionUseCase,
  private val deleteGameFromCollectionUseCase: DeleteGameFromCollectionUseCase,
  private val isGameInCollectionUseCase: IsGameInCollectionUseCase,
  private val addGameToWishListUseCase: AddGameToWishListUseCase,
  private val deleteGameFromWishListUseCase: DeleteGameFromWishListUseCase,
  private val isGameInWishListUseCase: IsGameInWishListUseCase,
  private val getListsWithThisGameUseCase: GetListsWithThisGameUseCase,
  private val getListsWithoutGameUseCase: GetListsWithoutGameUseCase,
  private val addGameToListsUseCase: AddGameToListsUseCase,
  private val deleteGameFromListsUseCase: DeleteGameFromListsUseCase
) : CommonBaseViewModel(), SpeedDialListener {

  private var gameId: String? = null

  private val isGameInCollectionLiveData = MutableLiveData<Boolean>()
  fun getIsGameInCollectionLiveData(): LiveData<Boolean> = isGameInCollectionLiveData

  private val isGameInWishListLiveData = MutableLiveData<Boolean>()
  fun getIsGameInWishListLiveData(): LiveData<Boolean> = isGameInWishListLiveData

  private val listsWithoutGameLiveData = MutableLiveData<List<GameList>>()
  fun getListsWithoutGameLiveData(): LiveData<List<GameList>> = listsWithoutGameLiveData

  private val listsWithThisGameLiveData = MutableLiveData<List<GameList>>()
  fun getListsWithThisGameLiveData(): LiveData<List<GameList>> = listsWithThisGameLiveData

  private val gameDetailLiveData = MutableLiveData<AsyncResult<GameDetail?>>()
  fun getGameDetailLiveData(): LiveData<AsyncResult<GameDetail?>> = gameDetailLiveData

  private val rowsDetailLiveData = MutableLiveData<List<DetailRow>>()
  fun getRowsDetailLiveData(): LiveData<List<DetailRow>> = rowsDetailLiveData

  fun initializeDetail(gameId: String) {
    this.gameId = gameId
    requestDetail()
    searchGameInCollection()
    searchGameInWishList()
    searchGameInLists()
  }

  private fun requestDetail() {
    gameId?.let {
      viewModelScope.launch {
        getGameDetailUseCase(it).collect { result ->
          gameDetailLiveData.value = result
          sendInvalidateMenuEvent()
          result.getSuccessData()?.let {
            rowsDetailLiveData.value = DetailRowManager.getDetailList(it)
          }
          stopLoading()
        }
      }
    }
  }

  private fun searchGameInCollection() {
    gameId?.let {
      viewModelScope.launch {
        isGameInCollectionUseCase(it).collect {
          isGameInCollectionLiveData.value = it
        }
      }
    }
  }

  private fun searchGameInWishList() {
    gameId?.let {
      viewModelScope.launch {
        isGameInWishListUseCase(it).collect {
          isGameInWishListLiveData.value = it
        }
      }
    }
  }

  private fun searchGameInLists() {
    gameId?.let {
      viewModelScope.launch {
        launch {
          getListsWithoutGameUseCase(it).collect {
            listsWithoutGameLiveData.value = it
          }
        }
        launch {
          getListsWithThisGameUseCase(it).collect {
            listsWithThisGameLiveData.value = it
          }
        }
      }
    }
  }

  override fun reload() {
    viewModelScope.launch {
      startLoading()
      delay(1000)
      requestDetail()
    }
  }

  //region SpeedDialListener
  override fun addGameToCollection() {
    gameDetailLiveData.value?.data?.let {
      viewModelScope.launch {
        addGameToCollectionUseCase(it).collect {
          it.sendEvents(
            this@GameDetailViewModel,
            GAME_ADDED_TO_COLLECTION,
            ADD_GAME_ERROR
          )
        }
      }
    }
  }

  override fun deleteGameFromCollection() {
    gameDetailLiveData.value?.data?. let {
      viewModelScope.launch {
        deleteGameFromCollectionUseCase(it).collect {
          it.sendEvents(
            this@GameDetailViewModel,
            GAME_DELETED_FROM_COLLECTION,
            DELETE_GAME_ERROR
          )
        }
      }
    }
  }

  override fun showListsToAdd() {
    sendDialogEvent(DialogState.SHOW_LISTS_TO_ADD)
  }

  override fun showListsToDelete() {
    sendDialogEvent(DialogState.SHOW_LISTS_TO_REMOVE)
  }
  //endregion

  private fun addGameToWishList() {
    gameDetailLiveData.value?.data?.let {
      viewModelScope.launch {
        addGameToWishListUseCase(it).collect {
          it.sendEvents(
            this@GameDetailViewModel,
            GAME_ADDED_TO_WISHLIST,
            ADD_GAME_ERROR
          )
        }
      }
    }
  }

  private fun deleteGameFromWishList() {
    gameDetailLiveData.value?.data?. let {
      viewModelScope.launch {
        deleteGameFromWishListUseCase(it).collect {
          it.sendEvents(
            this@GameDetailViewModel,
            GAME_DELETED_FROM_WISHLIST,
            DELETE_GAME_ERROR
          )
        }
      }
    }
  }

  fun isGameDetailLoaded(): Boolean {
    return gameDetailLiveData.value.isSuccess()
  }

  fun addGameToLists(index: List<Int>) {
    listsWithoutGameLiveData.value?.let { listsWithoutGame ->
      val listsToAdd = index.mapNotNull { listsWithoutGame.getOrNull(it) }
      gameDetailLiveData.value?.getSuccessData()?.let {
        viewModelScope.launch {
          addGameToListsUseCase(it, listsToAdd).sendEvents(
            this@GameDetailViewModel,
            GAME_ADDED_TO_LISTS,
            ADD_TO_LISTS_ERROR
          )
        }
      }
    }
  }

  fun deleteGameFromLists(index: List<Int>) {
    listsWithThisGameLiveData.value?.let { listsWithThisGame ->
      val listsToRemove = index.mapNotNull { listsWithThisGame.getOrNull(it) }
      gameDetailLiveData.value?.getSuccessData()?.let {
        viewModelScope.launch {
          deleteGameFromListsUseCase(it, listsToRemove).sendEvents(
            this@GameDetailViewModel,
            GAME_DELETED_FROM_LISTS,
            DELETE_FROM_LISTS_ERROR
          )
        }
      }
    }
  }

  fun onFavoriteClicked() {
    if (isGameInWishListLiveData.value.isTrue()) {
      deleteGameFromWishList()
    } else {
      addGameToWishList()
    }
  }
}