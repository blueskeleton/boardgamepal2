package com.example.boardgamepal.common.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
  tableName = "lists",
  indices = [
    Index(
      "list_name",
      unique = true)
  ])
data class GameListDbo(
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "list_id") val listId: Long = 0,
  @ColumnInfo(name = "list_name") val name: String
)
