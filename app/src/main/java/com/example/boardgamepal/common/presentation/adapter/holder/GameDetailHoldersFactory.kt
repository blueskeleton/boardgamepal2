package com.example.boardgamepal.common.presentation.adapter.holder

import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.BR
import com.example.boardgamepal.R
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.presentation.utils.DetailRow
import com.example.boardgamepal.common.presentation.viewmodel.GameDetailViewModel
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.common.utils.extension.getLifecycleOwner
import com.example.boardgamepal.databinding.RowDetailCategoriesBinding
import com.example.boardgamepal.databinding.RowDetailCreatorsBinding
import com.example.boardgamepal.databinding.RowDetailDescriptionBinding
import com.example.boardgamepal.databinding.RowDetailMainBinding
import com.example.boardgamepal.databinding.RowDetailMechanicsBinding
import com.example.boardgamepal.databinding.RowDetailPlayersBinding
import com.example.boardgamepal.databinding.RowDetailTimeBinding

class GameDetailHoldersFactory {

  internal fun getItemViewType(detailRow: DetailRow): Int {
    return detailRow.viewType
  }

  internal fun onCreateViewHolder(parameters: GameDetailHoldersFactoryParameters): RecyclerView.ViewHolder {
    val layoutInflater = parameters.parent.getLayoutInflater()

    val binding: ViewDataBinding? = when (parameters.viewType) {
      DetailRow.MAIN.viewType -> RowDetailMainBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.PLAYERS.viewType -> RowDetailPlayersBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.TIME.viewType -> RowDetailTimeBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.CREATORS.viewType -> RowDetailCreatorsBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.DESCRIPTION.viewType -> RowDetailDescriptionBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.MECHANICS.viewType -> RowDetailMechanicsBinding.inflate(layoutInflater, parameters.parent, false)

      DetailRow.CATEGORIES.viewType -> RowDetailCategoriesBinding.inflate(layoutInflater, parameters.parent, false)

      else -> null
    }

    return if (binding != null) {
      if (parameters.viewType == DetailRow.MAIN.viewType) {
        RowDetailMainViewHolder(binding as RowDetailMainBinding, parameters.detail, parameters.viewModel)
      } else {
        RowDetailBaseViewHolder(binding, parameters.detail)
      }
    } else {
      EmptyViewHolder(layoutInflater.inflate(R.layout.row_detail_empty, parameters.parent, false))
    }
  }

  internal fun onBindViewHolder(
    holder: RecyclerView.ViewHolder
  ) {
    (holder as? RowDetailBaseViewHolder)?.bind()
    (holder as? RowDetailMainViewHolder)?.bind()
  }

  //region ViewHolder
  inner class RowDetailBaseViewHolder(
    private val binding: ViewDataBinding,
    private val detail: GameDetail
  ) : RecyclerView.ViewHolder(binding.root) {
    fun bind() {
      binding.setVariable(BR.detail, detail)
      binding.executePendingBindings()
    }
  }

  inner class RowDetailMainViewHolder(
    private val binding: RowDetailMainBinding,
    private val detail: GameDetail,
    private val viewModel: GameDetailViewModel
  ) : RecyclerView.ViewHolder(binding.root) {
    fun bind() {
      binding.detail = detail
      binding.viewModel = viewModel
      binding.lifecycleOwner = binding.root.context.getLifecycleOwner()
      binding.executePendingBindings()
    }
  }

  inner class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
  //endregion

  data class GameDetailHoldersFactoryParameters(
    val parent: ViewGroup,
    val viewType: Int,
    val detail: GameDetail,
    val viewModel: GameDetailViewModel
  )

}