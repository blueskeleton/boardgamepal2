package com.example.boardgamepal.common.utils

class Event<out T>(private val content: T) {

  private var hasBeenHandled = false

  @Synchronized
  fun getContentIfNotHandled(): T? {
    return content
      .takeIf { !hasBeenHandled }
      ?.also { hasBeenHandled = true }
  }

}