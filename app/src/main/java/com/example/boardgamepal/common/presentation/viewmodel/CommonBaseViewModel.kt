package com.example.boardgamepal.common.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.example.boardgamepal.common.presentation.navigation.NavigationCommand
import com.example.boardgamepal.common.presentation.utils.DialogState
import com.example.boardgamepal.common.presentation.utils.ScreenState
import com.example.boardgamepal.common.utils.Event
import com.example.boardgamepal.common.utils.extension.isTrue

open class CommonBaseViewModel : ViewModel() {

  private val navigation = MutableLiveData<Event<NavigationCommand>>()
  fun getNavigation(): LiveData<Event<NavigationCommand>> = navigation

  private val stateLiveData = MutableLiveData<Event<ScreenState>>()
  fun getStateLiveData(): LiveData<Event<ScreenState>> = stateLiveData

  private val loadingLiveData = MutableLiveData(false)
  fun getLoading(): LiveData<Boolean> = loadingLiveData

  fun navigate(directions: NavDirections) {
    navigation.value = Event(NavigationCommand.To(directions))
  }

  fun navigateBack() {
    navigation.value = Event(NavigationCommand.Back)
  }

  fun sendErrorEvent(state: ScreenState.Error) {
    stateLiveData.value = Event(state)
  }

  fun sendSuccessEvent(state: ScreenState.Success) {
    stateLiveData.value = Event(state)
  }

  fun sendDialogEvent(type: DialogState) {
    stateLiveData.value = Event(ScreenState.Dialog(type))
  }

  fun sendInvalidateMenuEvent() {
    stateLiveData.value = Event(ScreenState.InvalidateMenu)
  }

  fun isReloading(): Boolean {
    return loadingLiveData.value.isTrue()
  }

  fun startLoading() {
    loadingLiveData.value = true
  }

  fun stopLoading() {
    loadingLiveData.value = false
  }

  open fun reload() {/* no-op */}

}