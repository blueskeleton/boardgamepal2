package com.example.boardgamepal.common.data.local.datasource

import androidx.room.*
import com.example.boardgamepal.common.data.local.model.GameDbo
import com.example.boardgamepal.common.data.local.model.GameListAndGameCrossRefDbo
import com.example.boardgamepal.common.data.local.model.GameListDbo
import com.example.boardgamepal.common.data.local.model.ListWithGamesDbo
import kotlinx.coroutines.flow.Flow

@Dao
abstract class GameListDao : BaseDao<GameListDbo>() {

  @Query("SELECT * FROM lists")
  abstract fun getAllLists(): Flow<List<ListWithGamesDbo>>

  @Query("SELECT L.* FROM lists as L " +
      "LEFT JOIN gameListAndGame as LG " +
      "ON LG.game_list_id = L.list_id AND LG.game_id = :gameId " +
      "WHERE LG.game_id IS NULL")
  abstract fun getListsWithoutGame(gameId: String): Flow<List<GameListDbo>>

  @Query("SELECT L.* FROM lists as L " +
      "INNER JOIN gameListAndGame as LG " +
      "ON LG.game_list_id = L.list_id AND LG.game_id = :gameId")
  abstract fun getListsWithThisGame(gameId: String): Flow<List<GameListDbo>>

  suspend fun createList(list: GameListDbo): Long {
    return insert(list)
  }

  suspend fun updateList(list: GameListDbo): Int {
    return update(list)
  }

  suspend fun deleteList(list: GameListDbo): Int {
    return delete(list)
  }

  @Transaction
  open suspend fun addGameToLists(game: GameDbo, lists: List<GameListAndGameCrossRefDbo>): List<Long> {
    insertGame(game)
    return insertCrossRefs(lists)
  }

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  abstract suspend fun insertCrossRefs(items: List<GameListAndGameCrossRefDbo>): List<Long>

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  abstract suspend fun insertGame(item: GameDbo)

  @Delete
  abstract fun deleteGameFromLists(game: GameDbo, lists: List<GameListAndGameCrossRefDbo>): Int

}