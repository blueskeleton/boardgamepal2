package com.example.boardgamepal.common.data.repository.di

import com.example.boardgamepal.common.data.local.datasource.GameLocalDataSource
import com.example.boardgamepal.common.data.remote.datasource.GameRemoteDataSource
import com.example.boardgamepal.common.data.repository.GameRepositoryImpl
import com.example.boardgamepal.common.domain.repository.GameRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

  @Provides
  fun gameRepositoryProvider(remote: GameRemoteDataSource, local: GameLocalDataSource): GameRepository {
    return GameRepositoryImpl(remote, local)
  }

}