package com.example.boardgamepal.common.data.local.datasource

import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames
import kotlinx.coroutines.flow.Flow

interface GameLocalDataSource {
  // CollectionDAO
  suspend fun getAllCollection(): Flow<List<Game>>
  suspend fun addGameToCollection(game: Game): Long
  suspend fun deleteGameFromCollection(game: Game): Int
  suspend fun isGameInCollection(gameId: String): Flow<String?>

  // WishListDAO
  suspend fun getAllWishList(): Flow<List<Game>>
  suspend fun addGameToWishList(game: Game): Long
  suspend fun deleteGameFromWishList(game: Game): Int
  suspend fun isGameInWishList(gameId: String): Flow<String?>

  // GameListDAO
  suspend fun getAllLists(): Flow<List<ListWithGames>>
  suspend fun getListsWithoutGame(gameId: String): Flow<List<GameList>>
  suspend fun getListsWithThisGame(gameId: String): Flow<List<GameList>>
  suspend fun addGameList(name: String): Long
  suspend fun updateGameList(list: GameList): Int
  suspend fun deleteGameList(list: GameList): Int
  suspend fun addGameToLists(game: Game, lists: List<GameList>): List<Long>
  suspend fun deleteGameFromLists(game: Game, lists: List<GameList>): Int
}