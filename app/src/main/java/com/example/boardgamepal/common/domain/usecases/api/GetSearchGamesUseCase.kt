package com.example.boardgamepal.common.domain.usecases.api

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class GetSearchGamesUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    name: String,
    currentPage: Int = 1
  ) = gameRepository.getGamesSearch(name, currentPage).flow()
}