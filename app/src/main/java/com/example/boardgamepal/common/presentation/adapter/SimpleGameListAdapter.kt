package com.example.boardgamepal.common.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.databinding.RowGameItemBinding

class SimpleGameAdapter(
  private val viewModel: CommonBaseViewModel
) : ListAdapter<Game, SimpleGame>(SimpleGameDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleGame {
    return SimpleGame(RowGameItemBinding.inflate(parent.getLayoutInflater(), parent, false), viewModel)
  }

  override fun onBindViewHolder(holder: SimpleGame, position: Int) {
    holder.bind(getItem(position))
  }

}

class SimpleGame(
  private val binding: RowGameItemBinding,
  private val viewModel: CommonBaseViewModel
) : RecyclerView.ViewHolder(binding.root) {
  fun bind(item: Game) {
    binding.game = item
    binding.viewModel = viewModel
  }
}

class SimpleGameDiffCallback : DiffUtil.ItemCallback<Game>() {
  override fun areItemsTheSame(oldItem: Game, newItem: Game): Boolean {
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: Game, newItem: Game): Boolean {
    return false
  }
}