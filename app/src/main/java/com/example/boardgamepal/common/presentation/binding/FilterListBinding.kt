package com.example.boardgamepal.common.presentation.binding

import androidx.databinding.BindingAdapter
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.viewmodel.FilterListViewModel
import com.example.boardgamepal.common.presentation.vo.SortByDirection
import com.example.boardgamepal.common.presentation.vo.SortByType
import com.google.android.material.chip.ChipGroup
import com.google.android.material.slider.RangeSlider
import com.google.android.material.slider.Slider

object FilterListBinding {

  @BindingAdapter("bind:onPlayersSlider")
  @JvmStatic
  fun onPlayersSlider(view: Slider, viewModel: FilterListViewModel?) {
    if (viewModel != null) {
      view.addOnChangeListener { slider, _, _ ->
        viewModel.setPlayersFilter(slider.value)
      }
    }
  }

  @BindingAdapter("bind:onPlaytimeSlider")
  @JvmStatic
  fun onPlaytimeSlider(view: Slider, viewModel: FilterListViewModel?) {
    if (viewModel != null) {
      view.addOnChangeListener { slider, _, _ ->
        viewModel.setPlaytimeFilter(slider.value)
      }
    }
  }

  @BindingAdapter("bind:onRatingSlider")
  @JvmStatic
  fun onRatingSlider(view: RangeSlider, viewModel: FilterListViewModel?) {
    if (viewModel != null) {
      view.addOnChangeListener { slider, _, _ ->
        viewModel.setRatingFilter(slider.values)
      }
    }
  }

  @BindingAdapter("bind:onSortByTypeGroup")
  @JvmStatic
  fun onSortByTypeGroup(view: ChipGroup, viewModel: FilterListViewModel?) {
    if (viewModel != null) {
      viewModel.getCurrentFilterLiveData().value?.let {
        when (it.getSortByValue()) {
          SortByType.NAME -> view.check(R.id.chip_name)
          SortByType.RATING -> view.check(R.id.chip_rating)
        }
      }

      view.setOnCheckedStateChangeListener { _, checkedIds ->
        val sortType = when (checkedIds.firstOrNull()) {
          R.id.chip_name -> SortByType.NAME
          R.id.chip_rating -> SortByType.RATING
          else -> null
        }

        sortType?.let { viewModel.setSortingType(it) }
      }
    }
  }

  @BindingAdapter("bind:onSortByDirectionGroup")
  @JvmStatic
  fun onSortByDirectionGroup(view: ChipGroup, viewModel: FilterListViewModel?) {
    if (viewModel != null) {
      viewModel.getCurrentFilterLiveData().value?.let {
        when (it.getDirectionValue()) {
          SortByDirection.ASCENDING -> view.check(R.id.chip_ascending)
          SortByDirection.DESCENDING -> view.check(R.id.chip_descending)
        }
      }

      view.setOnCheckedStateChangeListener { _, checkedIds ->
        val sortDirection = when (checkedIds.firstOrNull()) {
          R.id.chip_ascending -> SortByDirection.ASCENDING
          R.id.chip_descending -> SortByDirection.DESCENDING
          else -> null
        }

        sortDirection?.let { viewModel.setSortingDirection(it) }
      }
    }
  }

}