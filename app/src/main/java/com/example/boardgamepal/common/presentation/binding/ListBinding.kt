package com.example.boardgamepal.common.presentation.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.R
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.common.utils.extension.setCenteredImage
import com.example.boardgamepal.common.utils.extension.setCenteredImageWithPlaceholder
import com.example.boardgamepal.mylists.presentation.adapter.ListWithGamesAdapter
import com.example.boardgamepal.mylists.presentation.model.ListWithGamesVO
import com.example.boardgamepal.mylists.presentation.ui.MyListsFragmentDirections
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsViewModel
import com.google.android.material.card.MaterialCardView

object ListBinding {

  @BindingAdapter(value = [
    "bind:listWithGamesAdapter",
    "bind:myListsViewModel"
  ])
  @JvmStatic
  fun listWithGamesAdapter(
    view: RecyclerView,
    lists: List<ListWithGamesVO>?,
    viewModel: MyListsViewModel?
  ) {
    if (view.adapter == null && viewModel != null) {
      view.adapter = ListWithGamesAdapter(viewModel)
      view.setHasFixedSize(true)
    }

    (view.adapter as? ListWithGamesAdapter)?.submitList(lists)
  }

  @BindingAdapter(value = [
    "bind:goToListDetail",
    "bind:commonBaseViewModel"
  ])
  @JvmStatic
  fun goToListDetail(
    view: View,
    list: GameList?,
    viewModel: CommonBaseViewModel?
  ) {
    if (list != null && viewModel != null) {
      view.setOnClickListener {
        val navDirections = MyListsFragmentDirections.actionGoToListDetail(list.id, list.name)
        viewModel.navigate(navDirections)
      }
    }
  }

  @BindingAdapter("bind:gameResultsText")
  @JvmStatic
  fun gameResultsText(view: TextView, size: Int?) {
    view.apply {
      isVisible = size != null
      text = if (size == 0) {
        context.getString(R.string.no_game_results)
      } else {
        context.resources.getQuantityString(R.plurals.game_results, size ?: 0, size)
      }
    }
  }

  @BindingAdapter("bind:loadMainFourViewImage")
  @JvmStatic
  fun loadMainFourViewImage(view: ImageView, url: String?) {
    view.setCenteredImageWithPlaceholder(url)
  }

  @BindingAdapter("bind:loadFourViewImage")
  @JvmStatic
  fun loadFourViewImage(view: ImageView, url: String?) {
    url?.let {
      view.setCenteredImage(it)
      view.setBackgroundColor(view.resources.getColor(R.color.white))
    }
  }

  @BindingAdapter("bind:selectList")
  @JvmStatic
  fun selectList(view: MaterialCardView, selected: Boolean?) {
    view.strokeColor = view.resources.getColor(if (selected.isTrue()) {
      R.color.stroke_four_image_selected
    } else {
      R.color.stroke_four_image
    })

    view.strokeWidth = view.resources.getDimension(if (selected.isTrue()) {
      R.dimen.stroke_four_image_selected
    } else {
      R.dimen.stroke_four_image
    }).toInt()
  }

}