package com.example.boardgamepal.common.data.remote.utils

object ApiConstants {
  const val BASE_ENDPOINT = "https://api.boardgameatlas.com/api/"
  const val SEARCH_ENDPOINT = "search"
  const val DETAIL_ENDPOINT = "additional-info"
  const val MOCK_CLIENT_ID = "0mcQeA8goT"
  const val DEFAULT_PAGE_SIZE = 25
}

object ApiParameters {
  const val CLIENT_ID = "client_id"
  const val GAME_ID = "game_id"
  const val NAME = "name"
  const val ORDER_BY = "order_by"
  const val LIMIT = "limit"
  const val SKIP = "skip"
}

object OrderBy {
  const val RANK = "rank"
}