package com.example.boardgamepal.common.data.model

class CoreException(val asyncError: AsyncError) : Exception() {
  override fun toString(): String {
    return asyncError.toString()
  }
}