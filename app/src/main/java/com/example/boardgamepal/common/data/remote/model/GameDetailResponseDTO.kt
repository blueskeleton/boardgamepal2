package com.example.boardgamepal.common.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GameDetailResponseDTO(
  @Json(name = "game") val game: GameDetailDTO?
)
