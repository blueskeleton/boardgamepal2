package com.example.boardgamepal.common.domain.model.game

import com.example.boardgamepal.common.data.remote.model.mappers.WRONG_MIN_AGE

data class GameDetail(
  val id: String,
  val name: String,
  val description: String,
  val cover: Image,
  val minAge: Int,
  val yearPublished: Int,
  val numPlayers: PlayersRange,
  val numPlaytime: PlaytimeRange,
  val rating: Rating,
  val rank: Int,
  val designers: List<String>,
  val artists: List<String>,
  val mechanics: List<String>,
  val categories: List<String>
) {
  val yearPublishedText by lazy { yearPublished.toString() }
  val minAgeText by lazy { "$minAge+" }
  val designersText by lazy { designers.joinToString() }
  val artistsText by lazy { artists.joinToString() }

  val isMinAgeValid by lazy { minAge != WRONG_MIN_AGE }
  val isDesignersValid by lazy { designers.isNotEmpty() }
  val isArtistsValid by lazy { artists.isNotEmpty() }
}
