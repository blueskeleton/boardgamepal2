package com.example.boardgamepal.common.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
  tableName = "gameListAndGame",
  primaryKeys = ["game_list_id", "game_id"],
  foreignKeys = [
    ForeignKey(
      entity = GameListDbo::class,
      parentColumns = ["list_id"],
      childColumns = ["game_list_id"],
      onDelete = ForeignKey.CASCADE
    ),
    ForeignKey(
      entity = GameDbo::class,
      parentColumns = ["game_id"],
      childColumns = ["game_id"],
      onDelete = ForeignKey.CASCADE
    )
  ]
)
data class GameListAndGameCrossRefDbo(
  @ColumnInfo(name = "game_list_id") val gameListId: Long,
  @ColumnInfo(name = "game_id") val gameId: String
)
