package com.example.boardgamepal.common.domain.model

import com.example.boardgamepal.common.data.remote.utils.ApiConstants.DEFAULT_PAGE_SIZE
import com.example.boardgamepal.common.domain.model.game.Game

data class GamePagination(
  val games: List<Game>,
  val pageNumber: Int,
  val nextFewGames: Int
) {
  companion object {
    val DEFAULT = GamePagination(listOf(), 0, DEFAULT_PAGE_SIZE)
  }
}
