package com.example.boardgamepal.common.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.boardgamepal.common.presentation.utils.FilterListHelper
import com.example.boardgamepal.common.presentation.utils.FilterListLocalSource.Companion.DEFAULT_SORTING
import com.example.boardgamepal.common.presentation.vo.FilterVO
import com.example.boardgamepal.common.presentation.vo.SortByDirection
import com.example.boardgamepal.common.presentation.vo.SortByType
import com.example.boardgamepal.common.presentation.vo.SortVO

open class FilterListViewModel : CommonBaseViewModel() {

  private val currentFilterLiveData = MutableLiveData(FilterListHelper())
  fun getCurrentFilterLiveData(): LiveData<FilterListHelper> = currentFilterLiveData

  fun setCurrentFilter(filter: FilterListHelper) {
    currentFilterLiveData.value = filter
  }

  fun resetFilters() {
    currentFilterLiveData.value?.resetFilters()
  }

  fun saveOldValues() {
    currentFilterLiveData.value?.saveOldValues()
  }

  fun resetOldValues() {
    currentFilterLiveData.value?.resetOldValues()
  }

  fun getFilters(): List<FilterVO> {
    return currentFilterLiveData.value?.getFilters().orEmpty()
  }

  fun getSorting(): SortVO {
    return currentFilterLiveData.value?.getSorting()
      ?: DEFAULT_SORTING
  }

  fun setPlayersFilter(value: Float) {
    currentFilterLiveData.value?.setPlayersFilter(value)
  }

  fun setPlaytimeFilter(value: Float) {
    currentFilterLiveData.value?.setPlaytimeFilter(value)
  }

  fun setRatingFilter(values: List<Float>) {
    currentFilterLiveData.value?.setRatingFilter(values)
  }

  fun setSortingType(type: SortByType) {
    currentFilterLiveData.value?.setSortingType(type)
  }

  fun setSortingDirection(direction: SortByDirection) {
    currentFilterLiveData.value?.setSortingDirection(direction)
  }

}