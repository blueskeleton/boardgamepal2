package com.example.boardgamepal.common.presentation.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import com.example.boardgamepal.common.presentation.navigation.NavigationCommand
import com.example.boardgamepal.common.presentation.utils.DialogState
import com.example.boardgamepal.common.presentation.utils.ScreenState
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.invalidateMenu
import com.example.boardgamepal.common.utils.extension.makeToast
import com.example.boardgamepal.common.utils.extension.showSnackbar

abstract class CommonBaseFragment : Fragment() {

  var menu: Menu? = null

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    observeNavigation(getCommonViewModel())
    observeScreenEvent(getCommonViewModel())
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setHasOptionsMenu(true)
    initializeView()
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    getMenuResource()?.let {
      inflater.inflate(it, menu)
      this.menu = menu
    } ?: super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return onMenuItemSelected(item)
        || item.onNavDestinationSelected(findNavController())
        || super.onOptionsItemSelected(item)
  }

  abstract fun getCommonViewModel(): CommonBaseViewModel
  open fun getMenuResource(): Int? = null
  open fun getAboveSnackbarView(): View? = null
  open fun onMenuItemSelected(item: MenuItem): Boolean = false
  open fun initializeView() {/* no-op */}
  open fun createDialog(type: DialogState) {/* no-op */}

  private fun observeScreenEvent(viewModel: CommonBaseViewModel) {
    viewModel.getStateLiveData().observe(viewLifecycleOwner) {
       it.getContentIfNotHandled()?.let { state ->
        when (state) {
          is ScreenState.Error -> makeToast(state.messageRes)
          is ScreenState.Success -> {
            getAboveSnackbarView()?.showSnackbar(state.messageRes)
              ?: showSnackbar(state.messageRes)
          }
          is ScreenState.Dialog -> createDialog(state.type)
          is ScreenState.InvalidateMenu -> invalidateMenu()
        }
      }
    }
  }

  private fun observeNavigation(viewModel: CommonBaseViewModel) {
    viewModel.getNavigation().observe(viewLifecycleOwner) {
      it.getContentIfNotHandled()?.let { command ->
        when (command) {
          is NavigationCommand.To -> findNavController().navigate(command.directions)
          is NavigationCommand.Back -> navigateBack()
        }
      }
    }
  }

  private fun navigateBack() {
    try {
      findNavController().navigateUp()
    } catch (e: Exception) {
      activity?.onBackPressed()
    }
  }

}