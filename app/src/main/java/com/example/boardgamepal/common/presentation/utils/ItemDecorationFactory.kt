package com.example.boardgamepal.common.presentation.utils

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.boardgamepal.R

object ItemDecorationFactory {

  @JvmStatic
  fun buildLineDivider(context: Context): DividerItemDecoration {
    return DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
      ContextCompat.getDrawable(context, R.drawable.simple_divider)?.let { setDrawable(it) }
    }
  }

}