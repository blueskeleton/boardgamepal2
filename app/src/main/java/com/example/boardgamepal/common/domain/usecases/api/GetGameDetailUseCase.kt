package com.example.boardgamepal.common.domain.usecases.api

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class GetGameDetailUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    gameId: String
  ) = gameRepository.getGameDetail(gameId).flow()
}