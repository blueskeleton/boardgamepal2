package com.example.boardgamepal.common.utils.extension

import android.content.Context
import android.content.ContextWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar

fun View.getLayoutInflater(): LayoutInflater = LayoutInflater.from(this.context)

fun View.hideKeyboard() {
  val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
  imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun Context?.getLifecycleOwner(): LifecycleOwner? {
  return this as? LifecycleOwner ?: (this as? ContextWrapper)?.baseContext?.getLifecycleOwner()
}

fun View.showSnackbar(@StringRes stringRes: Int) {
  Snackbar.make(this, stringRes, Snackbar.LENGTH_SHORT).setAnchorView(this).show()
}

fun Fragment.showSnackbar(@StringRes stringRes: Int) {
  this.view?.let { Snackbar.make(it, stringRes, Snackbar.LENGTH_SHORT).show() }
}

fun Fragment.makeToast(@StringRes stringRes: Int) {
  Toast.makeText(this.requireContext(), stringRes, Toast.LENGTH_SHORT).show()
}

fun Fragment.getFilterListForResult() =
  findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>("APPLY_FILTERS")

fun Fragment.setFilterListForResult(applyFilters: Boolean) {
  findNavController().previousBackStackEntry?.savedStateHandle?.set("APPLY_FILTERS", applyFilters)
}