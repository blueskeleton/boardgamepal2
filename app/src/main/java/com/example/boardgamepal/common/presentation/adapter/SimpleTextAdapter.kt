package com.example.boardgamepal.common.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.databinding.RowSimpleTextBinding

class SimpleTextAdapter : ListAdapter<String, TextViewHolder>(SimpleTextDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextViewHolder {
    return TextViewHolder(RowSimpleTextBinding.inflate(parent.getLayoutInflater(), parent, false))
  }

  override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
    holder.bind(getItem(position))
  }

}

class TextViewHolder(
  private val binding: RowSimpleTextBinding
) : RecyclerView.ViewHolder(binding.root) {
  fun bind(item: String) {
    binding.item = item
  }
}

class SimpleTextDiffCallback : DiffUtil.ItemCallback<String>() {
  override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
    return oldItem == newItem
  }

  override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
    return false
  }
}