package com.example.boardgamepal.common.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ExtraGameDetailDTO(
  @Json(name = "minAge") val minAge: Int?,
  @Json(name = "cards") val cards: List<CardsDTO>?,
)
