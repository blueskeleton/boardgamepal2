package com.example.boardgamepal.common.presentation.listener

interface GamePaginationListener {
  fun loadMoreItems()
  fun isLastPage(): Boolean
  fun isLoading(): Boolean
}