package com.example.boardgamepal.common.data.local.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class ListWithGamesDbo(
  @Embedded val list: GameListDbo,
  @Relation(
    entity = GameDbo::class,
    parentColumn = "list_id",
    entityColumn = "game_id",
    associateBy = Junction(
      GameListAndGameCrossRefDbo::class,
      parentColumn = "game_list_id",
      entityColumn = "game_id"
    )
  )
  val games: List<GameDbo>
)
