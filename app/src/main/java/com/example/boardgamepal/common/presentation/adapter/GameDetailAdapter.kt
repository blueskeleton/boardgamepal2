package com.example.boardgamepal.common.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.presentation.adapter.holder.GameDetailHoldersFactory
import com.example.boardgamepal.common.presentation.utils.DetailRow
import com.example.boardgamepal.common.presentation.viewmodel.GameDetailViewModel

class GameDetailAdapter(
  private val detail: GameDetail,
  private val viewModel: GameDetailViewModel,
  private val holdersFactory: GameDetailHoldersFactory
) : ListAdapter<DetailRow, RecyclerView.ViewHolder>(GameDetailDiffCallback()) {

  //region Adapter
  override fun getItemViewType(position: Int): Int {
    return holdersFactory.getItemViewType(getItem(position))
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    val parameters = GameDetailHoldersFactory.GameDetailHoldersFactoryParameters(parent, viewType, detail, viewModel)
    return holdersFactory.onCreateViewHolder(parameters)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    holdersFactory.onBindViewHolder(holder)
  }
  //endregion

}

//region DiffCallback
private class GameDetailDiffCallback : DiffUtil.ItemCallback<DetailRow>() {

  override fun areItemsTheSame(oldItem: DetailRow, newItem: DetailRow): Boolean =
    oldItem.viewType == newItem.viewType

  override fun areContentsTheSame(oldItem: DetailRow, newItem: DetailRow): Boolean = false

}
//endregion