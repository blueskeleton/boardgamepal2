package com.example.boardgamepal.common.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GameResponseDTO(
  @Json(name = "games") val games: List<GameDTO>?,
  @Json(name = "count") val count: Int?,
)
