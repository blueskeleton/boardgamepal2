package com.example.boardgamepal.common.domain.usecases.api

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class GetTopGamesPaginationUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    currentPage: Int = 1
  ) = gameRepository.getTopGamesPagination(currentPage).flow()
}