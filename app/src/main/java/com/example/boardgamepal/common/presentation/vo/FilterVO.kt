package com.example.boardgamepal.common.presentation.vo

sealed class FilterVO(val type: FilterByType) {
  data class Players(val currentPlayers: Int? = null) : FilterVO(FilterByType.PLAYERS)
  data class Playtime(val currentPlaytime: Int? = null) : FilterVO(FilterByType.PLAYTIME)
  data class Rating(val min: Float? = null, val max: Float? = null) : FilterVO(FilterByType.RATING)
}

enum class FilterByType {
  PLAYERS,
  PLAYTIME,
  RATING
}
