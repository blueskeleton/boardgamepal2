package com.example.boardgamepal.common.presentation.binding

import android.graphics.PorterDuff
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.R
import com.example.boardgamepal.common.data.remote.utils.ApiConstants
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.Rating
import com.example.boardgamepal.common.presentation.InfiniteScrollListener
import com.example.boardgamepal.common.presentation.adapter.GameListAdapter
import com.example.boardgamepal.common.presentation.adapter.SimpleGameAdapter
import com.example.boardgamepal.common.presentation.adapter.holder.GameHoldersFactory
import com.example.boardgamepal.common.presentation.listener.GamePaginationListener
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.setThumbnail

object GamesBinding {

  @BindingAdapter(value = [
    "bind:gamesListAdapter",
    "bind:gamePaginationListener",
    "bind:commonBaseViewModel"
  ])
  @JvmStatic
  fun gamesListAdapter(
    view: RecyclerView,
    games: List<Game>?,
    listener: GamePaginationListener?,
    viewModel: CommonBaseViewModel?
  ) {
    if (view.adapter == null && listener != null && viewModel != null) {
      view.adapter = GameListAdapter(viewModel, GameHoldersFactory())
      view.setHasFixedSize(true)

      (view.layoutManager as? LinearLayoutManager)?.let {
        view.addOnScrollListener(
          InfiniteScrollListener(
          it,
          ApiConstants.DEFAULT_PAGE_SIZE,
          listener
        ))
      }
    }

    (view.adapter as? GameListAdapter)?.submitList(games)
  }

  @BindingAdapter(value = [
    "bind:simpleGameAdapter",
    "bind:commonBaseViewModel"
  ])
  @JvmStatic
  fun simpleGameAdapter(
    view: RecyclerView,
    games: List<Game>?,
    viewModel: CommonBaseViewModel?
  ) {
    if (view.adapter == null && viewModel != null) {
      view.adapter = SimpleGameAdapter(viewModel)
    }

    (view.adapter as? SimpleGameAdapter)?.submitList(games)
  }

  @BindingAdapter("bind:ratingBackground")
  @JvmStatic
  fun setRatingBackgroundColor(
    view: View,
    rating: Rating?
  ) {
    rating?.integerValue?.let {
      val colorId = when {
        it >= 8 -> R.color.rating_very_good
        it >= 7 -> R.color.rating_good
        it >= 5 -> R.color.rating_regular
        else -> R.color.rating_bad
      }
      view.background.setColorFilter(ContextCompat.getColor(view.context, colorId), PorterDuff.Mode.SRC_ATOP)
    }
  }

  @BindingAdapter("bind:loadThumbnail")
  @JvmStatic
  fun loadThumbnail(view: ImageView, game: Game?) {
    view.setThumbnail(game)
  }

}