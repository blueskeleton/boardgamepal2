package com.example.boardgamepal.common.presentation.vo

sealed class SortVO() {
  data class Ascending(val type: SortByType) : SortVO()
  data class Descending(val type: SortByType) : SortVO()
}

enum class SortByType {
  NAME,
  RATING
}

enum class SortByDirection {
  ASCENDING,
  DESCENDING
}