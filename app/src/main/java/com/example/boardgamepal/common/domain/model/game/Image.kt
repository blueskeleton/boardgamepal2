package com.example.boardgamepal.common.domain.model.game

data class Image(
  val small: String?,
  val large: String?
) {
  companion object {
    const val EMPTY_PHOTO = ""
    val DEFAULT = Image(EMPTY_PHOTO, EMPTY_PHOTO)
  }
}
