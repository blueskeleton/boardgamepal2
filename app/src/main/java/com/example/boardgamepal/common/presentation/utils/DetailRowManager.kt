package com.example.boardgamepal.common.presentation.utils

import com.example.boardgamepal.common.domain.model.game.GameDetail

object DetailRowManager {

  private fun getDefaultDetailRows(): List<DetailRow> {
    return listOf(
      DetailRow.MAIN,
      DetailRow.PLAYERS,
      DetailRow.TIME,
      DetailRow.CREATORS,
      DetailRow.DESCRIPTION,
      DetailRow.MECHANICS,
      DetailRow.CATEGORIES
    )
  }

  fun getDetailList(detail: GameDetail, rows: List<DetailRow> = getDefaultDetailRows()): List<DetailRow> {
    return rows.filter {
      when (it) {
        DetailRow.PLAYERS -> detail.numPlayers.isValid || detail.isMinAgeValid
        DetailRow.TIME -> detail.numPlaytime.isValid
        DetailRow.CREATORS -> detail.isDesignersValid || detail.isArtistsValid
        DetailRow.DESCRIPTION -> detail.description.isNotEmpty()
        DetailRow.MECHANICS -> detail.mechanics.isNotEmpty()
        DetailRow.CATEGORIES -> detail.categories.isNotEmpty()
        DetailRow.IMAGES -> false
        else -> true
      }
    }
  }

}