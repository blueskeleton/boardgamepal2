package com.example.boardgamepal.common.data.remote.datasource

import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.GameDetail

interface GameRemoteDataSource {
  suspend fun getTopGames(pageNumber: Int): GamePagination
  suspend fun getGamesSearch(name: String, pageNumber: Int): GamePagination
  suspend fun getGameDetail(gameId: String): GameDetail?
}