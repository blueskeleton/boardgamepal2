package com.example.boardgamepal.common.data.remote.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DesignerDTO(
  @Json(name = "name") val name: String?
)