package com.example.boardgamepal.common.presentation.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.presentation.vo.FilterByType
import com.example.boardgamepal.common.presentation.vo.FilterVO
import com.example.boardgamepal.common.presentation.vo.SortByType
import com.example.boardgamepal.common.presentation.vo.SortVO
import com.example.boardgamepal.common.utils.extension.orTrue

class FilterListLocalSource {

  companion object {
    val DEFAULT_SORTING = SortVO.Ascending(SortByType.NAME)
  }

  private var originalList: List<Game> = listOf()
  private val filteredList: MutableLiveData<AsyncResult<List<Game>>> = MutableLiveData()

  private val filters: MutableMap<FilterByType, FilterVO> = mutableMapOf()
  private var sort: SortVO = DEFAULT_SORTING

  fun applyFilters() {
    var newList = originalList

    if (filters.isNotEmpty()) {
      val playersFilter = filters[FilterByType.PLAYERS] as? FilterVO.Players
      val playTimeFilter = filters[FilterByType.PLAYTIME] as? FilterVO.Playtime
      val ratingFilter = filters[FilterByType.RATING] as? FilterVO.Rating

      newList = newList.filter {
        it.filterByPlayers(playersFilter)
            && it.filterByPlaytime(playTimeFilter)
            && it.filterByRating(ratingFilter)
      }
    }

    newList = newList.getSorting()

    filteredList.postValue(AsyncResult.Success(newList))
  }

  private fun List<Game>.getSorting(): List<Game> {
    return sort.let { sorting ->
      when {
        sorting is SortVO.Ascending && sorting.type == SortByType.NAME -> {
          this.sortedBy { it.name }
        }
        sorting is SortVO.Descending && sorting.type == SortByType.NAME -> {
          this.sortedByDescending { it.name }
        }
        sorting is SortVO.Ascending && sorting.type == SortByType.RATING -> {
          this.sortedBy { it.rating.averageUserRating }
        }
        sorting is SortVO.Descending && sorting.type == SortByType.RATING -> {
          this.sortedByDescending { it.rating.averageUserRating }
        }
        else -> this
      }
    }
  }

  private fun Game.filterByPlayers(playersFilter: FilterVO.Players?): Boolean {
    return playersFilter?.currentPlayers?.let { this.numPlayers.contains(it) }.orTrue()
  }

  private fun Game.filterByPlaytime(playTimeFilter: FilterVO.Playtime?): Boolean {
    return playTimeFilter?.currentPlaytime?.let {
      this.numPlaytime.contains(it) || it > this.numPlaytime.maxPlaytime
    }.orTrue()
  }

  private fun Game.filterByRating(ratingFilter: FilterVO.Rating?): Boolean {
    return ratingFilter?.let { filter ->
      this.rating.contains(filter.min, filter.max)
    }.orTrue()
  }

  private fun List<FilterVO>.toMap(): Map<FilterByType, FilterVO> {
    return this.associateBy { it.type }
  }

  fun addFiltersAndSorting(filters: List<FilterVO>, sort: SortVO) {
    this.filters.clear()
    this.filters.putAll(filters.toMap())
    this.sort = sort
  }

  fun removeFilters() {
    filters.clear()
    sort = DEFAULT_SORTING
    applyFilters()
  }

  fun hasFilters(): Boolean {
    return filters.isNotEmpty() || sort != DEFAULT_SORTING
  }

  fun setLoading() {
    filteredList.postValue(AsyncResult.Loading(null))
  }

  fun setOriginalList(newList: List<Game>) {
    originalList = newList
    applyFilters()
  }

  fun isNotEmpty(): Boolean {
    return originalList.isNotEmpty()
  }

  fun liveData() = filteredList as LiveData<AsyncResult<List<Game>>>

}