package com.example.boardgamepal.common.domain.usecases.gamelists

import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class AddGameToListsUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    detail: GameDetail,
    lists: List<GameList>
  ) = gameRepository.addGameToLists(detail, lists).valueAsync().await()
}