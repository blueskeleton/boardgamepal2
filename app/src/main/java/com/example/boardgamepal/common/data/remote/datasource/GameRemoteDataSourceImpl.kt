package com.example.boardgamepal.common.data.remote.datasource

import com.example.boardgamepal.common.data.remote.error.RemoteErrorManagement
import com.example.boardgamepal.common.data.remote.model.mappers.toModel
import com.example.boardgamepal.common.data.remote.utils.ApiConstants
import com.example.boardgamepal.common.data.remote.utils.OrderBy
import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.GameDetail

class GameRemoteDataSourceImpl(
  private val api: GameWs
) : GameRemoteDataSource {
  override suspend fun getTopGames(pageNumber: Int): GamePagination {
    return RemoteErrorManagement.wrap {
      api.getTopGames(
        ApiConstants.MOCK_CLIENT_ID,
        OrderBy.RANK,
        (pageNumber - 1) * ApiConstants.DEFAULT_PAGE_SIZE
      ).toModel(pageNumber + 1)
    }
  }

  override suspend fun getGamesSearch(name: String, pageNumber: Int): GamePagination {
    return RemoteErrorManagement.wrap {
      api.getGamesSearch(
        ApiConstants.MOCK_CLIENT_ID,
        name,
        (pageNumber - 1) * ApiConstants.DEFAULT_PAGE_SIZE
      ).toModel(pageNumber)
    }
  }

  override suspend fun getGameDetail(gameId: String): GameDetail? {
    return RemoteErrorManagement.wrap {
      api.getGameDetail(
        ApiConstants.MOCK_CLIENT_ID,
        gameId
      ).toModel()
    }
  }

}