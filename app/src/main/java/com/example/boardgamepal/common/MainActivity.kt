package com.example.boardgamepal.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.boardgamepal.R
import com.example.boardgamepal.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

  private lateinit var binding: ActivityMainBinding

  // Navigation
  private lateinit var appBarConfiguration: AppBarConfiguration
  private lateinit var navController: NavController

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)
    setUpNavController()
    setUpToolbar()
  }

  override fun onSupportNavigateUp(): Boolean {
    return navController.navigateUp(appBarConfiguration)
  }

  private fun setUpNavController() {
    val navHostFragment = supportFragmentManager.findFragmentById(
      R.id.main_activity__container__navigation_fragment
    ) as NavHostFragment
    navController = navHostFragment.navController
    binding.navController = navController
  }

  private fun setUpToolbar() {
    setSupportActionBar(binding.toolbar)
    appBarConfiguration = AppBarConfiguration(
      setOf(
        R.id.explore_dest,
        R.id.my_collection_dest,
        R.id.wishlist_dest,
        R.id.my_lists_dest
      )
    )
    setupActionBarWithNavController(navController, appBarConfiguration)
  }
}