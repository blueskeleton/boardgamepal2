package com.example.boardgamepal.common.presentation.utils

enum class DetailRow(val viewType: Int) {
  MAIN(1),
  PLAYERS(2),
  TIME(3),
  CREATORS(4),
  DESCRIPTION(5),
  MECHANICS(6),
  CATEGORIES(7),
  IMAGES(8)
}