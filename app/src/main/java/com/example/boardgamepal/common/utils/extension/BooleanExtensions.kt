package com.example.boardgamepal.common.utils.extension

fun Boolean?.isTrue(): Boolean {
  return this == true
}

fun Boolean?.isFalse(): Boolean {
  return this == false
}

fun Boolean?.isNotTrue(): Boolean {
  return this != true
}

fun Boolean?.orTrue(): Boolean {
  return this ?: true
}