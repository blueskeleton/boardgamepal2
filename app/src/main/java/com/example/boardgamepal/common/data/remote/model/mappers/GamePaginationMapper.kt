package com.example.boardgamepal.common.data.remote.model.mappers

import com.example.boardgamepal.common.data.remote.model.GameResponseDTO
import com.example.boardgamepal.common.data.remote.utils.ApiConstants.DEFAULT_PAGE_SIZE
import com.example.boardgamepal.common.domain.model.GamePagination

fun GameResponseDTO.toModel(pageNumber: Int) = GamePagination(
  games = games?.mapNotNull { it.toBo() }.orEmpty(),
  pageNumber = pageNumber,
  nextFewGames = count?.let {
    (it - pageNumber * DEFAULT_PAGE_SIZE).coerceAtLeast(0)
  } ?: DEFAULT_PAGE_SIZE
)