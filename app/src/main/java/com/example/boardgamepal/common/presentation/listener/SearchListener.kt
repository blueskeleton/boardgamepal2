package com.example.boardgamepal.common.presentation.listener

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchListener(
  lifecycle: Lifecycle,
  private val doAfterTextChangedListener: (String) -> Unit
) {
  private val coroutineScope = lifecycle.coroutineScope
  private var searchJob: Job? = null

  fun onTextChanged(newText: String?, time: Long = 0) {
    searchJob?.cancel()
    if (newText != null && newText.length >= 3) {
      searchJob = coroutineScope.launch {
        delay(time)
        doAfterTextChangedListener(newText)
      }
    }
  }
}