package com.example.boardgamepal.common.domain.usecases.wishlist

import com.example.boardgamepal.common.domain.model.game.GameDetail
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class AddGameToWishListUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    detail: GameDetail
  ) = gameRepository.addGameToWishList(detail).flow()
}