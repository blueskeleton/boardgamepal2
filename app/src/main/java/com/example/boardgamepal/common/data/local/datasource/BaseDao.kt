package com.example.boardgamepal.common.data.local.datasource

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

abstract class BaseDao<T> {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  protected abstract suspend fun insert(items: List<T>)

  @Insert(onConflict = OnConflictStrategy.ABORT)
  protected abstract suspend fun insert(item: T): Long

  @Update
  protected abstract suspend fun update(items: List<T>): Int

  @Update
  protected abstract suspend fun update(item: T): Int

  @Delete
  protected abstract suspend fun delete(item: T): Int

  @Delete
  protected abstract suspend fun delete(items: List<T>)

}