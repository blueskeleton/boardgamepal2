package com.example.boardgamepal.common.utils.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.boardgamepal.R
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.GameDetail

fun ImageView.setThumbnail(game: Game?) {
  Glide.with(this.context)
    .load(game?.cover?.small?.ifEmpty { null })
    .error(R.drawable.no_image_available)
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)
}

fun ImageView.setMainDetailImage(game: GameDetail?) {
  Glide.with(this.context)
    .load(game?.cover?.small?.ifEmpty { null })
    .error(R.drawable.no_image_available)
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)
}

fun ImageView.setCenteredImage(url: String?) {
  Glide.with(this.context)
    .load(url?.ifEmpty { null })
    .centerInside()
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)
}

fun ImageView.setCenteredImageWithPlaceholder(url: String?) {
  Glide.with(this.context)
    .load(url?.ifEmpty { null })
    .error(R.drawable.no_image_available)
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)
}