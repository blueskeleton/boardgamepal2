package com.example.boardgamepal.common.presentation.utils

import com.example.boardgamepal.common.presentation.vo.FilterVO
import com.example.boardgamepal.common.presentation.vo.SortByDirection
import com.example.boardgamepal.common.presentation.vo.SortByType
import com.example.boardgamepal.common.presentation.vo.SortVO

class FilterListHelper {

  companion object {
    const val PLAYERS_INITIAL_VALUE = 0f
    const val PLAYTIME_INITIAL_VALUE = 1f
    const val RATING_INITIAL_VALUE = 1f
    const val RATING_FINAL_VALUE = 10f
  }

  // Sliders
  private var currentPlayers = PLAYERS_INITIAL_VALUE
  private var oldPlayers = PLAYERS_INITIAL_VALUE
  private var currentPlaytime = PLAYTIME_INITIAL_VALUE
  private var oldPlaytime = PLAYTIME_INITIAL_VALUE
  private var currentRating = listOf(RATING_INITIAL_VALUE, RATING_FINAL_VALUE)
  private var oldRating = listOf(RATING_INITIAL_VALUE, RATING_FINAL_VALUE)

  // ChipGroups
  private var currentSortBy = SortByType.NAME
  private var oldSortBy = SortByType.NAME
  private var currentDirection = SortByDirection.ASCENDING
  private var oldDirection = SortByDirection.ASCENDING

  fun getPlayersValue() = currentPlayers
  fun getPlaytimeValue() = currentPlaytime
  fun getRatingValues() = currentRating
  fun getSortByValue() = currentSortBy
  fun getDirectionValue() = currentDirection

  fun getFilters(): List<FilterVO> {
    return mutableListOf<FilterVO>().apply {
      if (currentPlayers != PLAYERS_INITIAL_VALUE) {
        add(FilterVO.Players(currentPlayers.toInt()))
      }

      if (currentPlaytime != PLAYTIME_INITIAL_VALUE) {
        add(FilterVO.Playtime(getRealPlaytime(currentPlaytime)?.toInt()))
      }

      val firstRating = currentRating.getOrNull(0)
      val finalRating = currentRating.getOrNull(1)

      if (firstRating != RATING_INITIAL_VALUE || finalRating != RATING_FINAL_VALUE) {
        add(FilterVO.Rating(
          firstRating?.takeIf { it != RATING_INITIAL_VALUE },
          finalRating?.takeIf { it != RATING_FINAL_VALUE }
        ))
      }
    }
  }

  fun getSorting(): SortVO = when (currentDirection) {
    SortByDirection.ASCENDING -> SortVO.Ascending(currentSortBy)
    SortByDirection.DESCENDING -> SortVO.Descending(currentSortBy)
  }

  fun resetFilters() {
    currentPlayers = PLAYERS_INITIAL_VALUE
    currentPlaytime = PLAYTIME_INITIAL_VALUE
    currentRating = listOf(RATING_INITIAL_VALUE, RATING_FINAL_VALUE)
    currentSortBy = SortByType.NAME
    currentDirection = SortByDirection.ASCENDING

    oldPlayers = PLAYERS_INITIAL_VALUE
    oldPlaytime = PLAYTIME_INITIAL_VALUE
    oldRating = listOf(RATING_INITIAL_VALUE, RATING_FINAL_VALUE)
    oldSortBy = SortByType.NAME
    oldDirection = SortByDirection.ASCENDING
  }

  fun saveOldValues() {
    oldPlayers = currentPlayers
    oldPlaytime = currentPlaytime
    oldRating = currentRating
    oldSortBy = currentSortBy
    oldDirection = currentDirection
  }

  fun resetOldValues() {
    currentPlayers = oldPlayers
    currentPlaytime = oldPlaytime
    currentRating = oldRating
    currentSortBy = oldSortBy
    currentDirection = oldDirection
  }

  fun setPlayersFilter(value: Float) {
    currentPlayers = value
  }

  fun setPlaytimeFilter(value: Float) {
    currentPlaytime = value
  }

  fun setRatingFilter(values: List<Float>) {
    currentRating = listOf(*values.toTypedArray())
  }

  fun setSortingType(type: SortByType) {
    currentSortBy = type
  }

  fun setSortingDirection(direction: SortByDirection) {
    currentDirection = direction
  }

  private fun getRealPlaytime(value: Float?): Float? {
    return when (value) {
      1f -> 0f
      2f -> 5f
      3f -> 10f
      4f -> 15f
      5f -> 30f
      6f -> 45f
      7f -> 60f
      8f -> 90f
      9f -> 120f
      10f -> 150f
      11f -> 180f
      12f -> 210f
      13f -> 240f
      14f -> 300f
      15f -> 360f
      else -> null
    }
  }

}