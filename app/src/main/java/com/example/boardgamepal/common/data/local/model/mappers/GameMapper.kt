package com.example.boardgamepal.common.data.local.model.mappers

import com.example.boardgamepal.common.data.local.model.CollectionGameDbo
import com.example.boardgamepal.common.data.local.model.GameDbo
import com.example.boardgamepal.common.data.local.model.WishListGameDbo
import com.example.boardgamepal.common.data.remote.model.mappers.parseNumPlayers
import com.example.boardgamepal.common.data.remote.model.mappers.parseNumPlaytime
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.Image
import com.example.boardgamepal.common.domain.model.game.Image.Companion.EMPTY_PHOTO
import com.example.boardgamepal.common.domain.model.game.Rating

fun CollectionGameDbo.toBo(): Game {
  return Game(
    id = id,
    name = name,
    numPlayers = parseNumPlayers(minPlayers, maxPlayers),
    numPlaytime = parseNumPlaytime(minPlaytime, maxPlaytime),
    cover = parseDatabaseImage(smallImage),
    rating = Rating(averageUserRating.toFloat())
  )
}

fun WishListGameDbo.toBo(): Game {
  return Game(
    id = id,
    name = name,
    numPlayers = parseNumPlayers(minPlayers, maxPlayers),
    numPlaytime = parseNumPlaytime(minPlaytime, maxPlaytime),
    cover = parseDatabaseImage(smallImage),
    rating = Rating(averageUserRating.toFloat())
  )
}

fun GameDbo.toBo(): Game {
  return Game(
    id = gameId,
    name = name,
    numPlayers = parseNumPlayers(minPlayers, maxPlayers),
    numPlaytime = parseNumPlaytime(minPlaytime, maxPlaytime),
    cover = parseDatabaseImage(smallImage),
    rating = Rating(averageUserRating.toFloat())
  )
}

fun Game.toCollectionDbo(): CollectionGameDbo {
  return CollectionGameDbo(
    id = id,
    name = name,
    minPlayers = numPlayers.minPlayers,
    maxPlayers = numPlayers.maxPlayers,
    minPlaytime = numPlaytime.minPlaytime,
    maxPlaytime = numPlaytime.maxPlaytime,
    smallImage = cover.small ?: EMPTY_PHOTO,
    averageUserRating = rating.averageUserRating.toDouble(),
  )
}

fun Game.toWishListDbo(): WishListGameDbo {
  return WishListGameDbo(
    id = id,
    name = name,
    minPlayers = numPlayers.minPlayers,
    maxPlayers = numPlayers.maxPlayers,
    minPlaytime = numPlaytime.minPlaytime,
    maxPlaytime = numPlaytime.maxPlaytime,
    smallImage = cover.small ?: EMPTY_PHOTO,
    averageUserRating = rating.averageUserRating.toDouble(),
  )
}

fun Game.toDbo(): GameDbo {
  return GameDbo(
    gameId = id,
    name = name,
    minPlayers = numPlayers.minPlayers,
    maxPlayers = numPlayers.maxPlayers,
    minPlaytime = numPlaytime.minPlaytime,
    maxPlaytime = numPlaytime.maxPlaytime,
    smallImage = cover.small ?: EMPTY_PHOTO,
    averageUserRating = rating.averageUserRating.toDouble(),
  )
}

fun parseDatabaseImage(smallImage: String): Image {
  return Image(small = smallImage, EMPTY_PHOTO)
}