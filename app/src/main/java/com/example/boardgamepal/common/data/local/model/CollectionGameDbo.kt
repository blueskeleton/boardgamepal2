package com.example.boardgamepal.common.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.boardgamepal.common.domain.model.game.Image.Companion.EMPTY_PHOTO
import com.example.boardgamepal.common.domain.model.game.WRONG_PLAYERS_NUMBER
import com.example.boardgamepal.common.domain.model.game.WRONG_PLAYTIME
import com.example.boardgamepal.common.domain.model.game.WRONG_RATING_D

@Entity(tableName = "collection")
data class CollectionGameDbo(
  @PrimaryKey(autoGenerate = false) val id: String,
  @ColumnInfo(name = "name") val name: String,
  @ColumnInfo(name = "min_players") val minPlayers: Int = WRONG_PLAYERS_NUMBER,
  @ColumnInfo(name = "max_players") val maxPlayers: Int = WRONG_PLAYERS_NUMBER,
  @ColumnInfo(name = "min_playtime") val minPlaytime: Int = WRONG_PLAYTIME,
  @ColumnInfo(name = "max_playtime") val maxPlaytime: Int = WRONG_PLAYTIME,
  @ColumnInfo(name = "small_image") val smallImage: String = EMPTY_PHOTO,
  @ColumnInfo(name = "average_user_rating") val averageUserRating: Double = WRONG_RATING_D
)