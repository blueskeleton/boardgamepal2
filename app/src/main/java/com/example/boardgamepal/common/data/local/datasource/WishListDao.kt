package com.example.boardgamepal.common.data.local.datasource

import androidx.room.Dao
import androidx.room.Query
import com.example.boardgamepal.common.data.local.model.WishListGameDbo
import kotlinx.coroutines.flow.Flow

@Dao
abstract class WishListDao : BaseDao<WishListGameDbo>() {

  @Query("SELECT * FROM wishlist")
  abstract fun getAllWishListGames(): Flow<List<WishListGameDbo>>

  @Query("SELECT id FROM wishlist WHERE id = :id LIMIT 1")
  abstract fun isGameInWishList(id: String): Flow<String?>

  suspend fun save(game: WishListGameDbo): Long {
    return insert(game)
  }

  suspend fun save(games: List<WishListGameDbo>) {
    insert(games)
  }

  suspend fun deleteGame(game: WishListGameDbo): Int {
    return delete(game)
  }

}