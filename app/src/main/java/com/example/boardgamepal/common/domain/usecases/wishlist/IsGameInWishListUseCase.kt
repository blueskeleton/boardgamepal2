package com.example.boardgamepal.common.domain.usecases.wishlist

import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class IsGameInWishListUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    gameId: String
  ) = gameRepository.isGameInWishList(gameId)
}