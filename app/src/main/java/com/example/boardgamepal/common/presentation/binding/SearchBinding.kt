package com.example.boardgamepal.common.presentation.binding

import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.BindingAdapter
import com.example.boardgamepal.common.presentation.listener.SearchListener
import com.example.boardgamepal.common.utils.extension.hideKeyboard

object SearchBinding {

  @BindingAdapter("bind:searchListener")
  @JvmStatic
  fun searchListener(
    view: EditText,
    searchListener: SearchListener
  ) {
    view.apply {
      doAfterTextChanged {
        searchListener.onTextChanged(it.toString(), 3000)
      }

      setOnEditorActionListener { textView, actionId, _ ->
        return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_SEARCH) {
          hideKeyboard()
          searchListener.onTextChanged(textView.text.toString())
          true
        } else {
          false
        }
      }
    }
  }

}