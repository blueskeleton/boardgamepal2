package com.example.boardgamepal.common.data.remote.model.mappers

import android.util.Log
import com.example.boardgamepal.common.data.remote.model.CategoryDTO
import com.example.boardgamepal.common.data.remote.model.DesignerDTO
import com.example.boardgamepal.common.data.remote.model.ExtraGameDetailDTO
import com.example.boardgamepal.common.data.remote.model.GameDetailDTO
import com.example.boardgamepal.common.data.remote.model.GameDetailResponseDTO
import com.example.boardgamepal.common.data.remote.model.MechanicDTO
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.game.GameDetail

const val EMPTY_STRING = ""
const val WRONG_YEAR = -1
const val WRONG_RANK = -1
const val WRONG_MIN_AGE = -1

fun GameDetailResponseDTO.toModel() = game?.toBo()

fun GameDetailDTO.toBo(): GameDetail? {
  return try {
    GameDetail(
      id = id ?: throw MappingException("GameDetail id cannot be null"),
      name = name ?: throw MappingException("GameDetail name cannot be null"),
      description = description ?: EMPTY_STRING,
      cover = parseImage(image),
      minAge = parseMinAge(extraDetails),
      yearPublished = yearPublished ?: WRONG_YEAR,
      numPlayers = parseNumPlayers(minPlayers, maxPlayers),
      numPlaytime = parseNumPlaytime(minPlaytime, maxPlaytime),
      rating = parseRating(averageUserRating),
      rank = rank ?: WRONG_RANK,
      designers = parseDesigners(designers),
      artists = artists.orEmpty(),
      mechanics = parseMechanics(mechanics),
      categories = parseCategories(categories)
    )
  } catch (e: Exception) {
    Log.e("GameDetailMapper", e.message ?: "Error mapping GameDetailDTO")
    null
  }
}

fun GameDetail.toGame(): Game {
  return Game(
      id = id,
      name = name,
      numPlayers = numPlayers,
      numPlaytime = numPlaytime,
      cover = cover,
      rating = rating
    )
}

fun parseMinAge(extraDetails: ExtraGameDetailDTO?) = extraDetails?.minAge ?: WRONG_MIN_AGE

fun parseDesigners(designers: List<DesignerDTO>?) = designers?.mapNotNull { it.name }.orEmpty()

fun parseMechanics(mechanics: List<MechanicDTO>?) = mechanics?.mapNotNull { it.name }.orEmpty()

fun parseCategories(categories: List<CategoryDTO>?) = categories?.mapNotNull { it.name }.orEmpty()