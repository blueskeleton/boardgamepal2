package com.example.boardgamepal.common.domain.usecases.gamelists
import com.example.boardgamepal.common.domain.repository.GameRepository
import javax.inject.Inject

class GetListsWithoutGameUseCase @Inject constructor(
  private val gameRepository: GameRepository
) {
  suspend operator fun invoke(
    gameId: String
  ) = gameRepository.getListsWithoutGame(gameId)
}