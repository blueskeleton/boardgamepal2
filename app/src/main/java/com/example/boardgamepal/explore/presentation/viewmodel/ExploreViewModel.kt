package com.example.boardgamepal.explore.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.data.repository.util.getSuccessData
import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.usecases.api.GetTopGamesPaginationUseCase
import com.example.boardgamepal.common.presentation.listener.GamePaginationListener
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.generateList
import com.example.boardgamepal.common.utils.extension.orDefaultPage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExploreViewModel @Inject constructor(
  private val getTopGamesPaginationUseCase: GetTopGamesPaginationUseCase
): CommonBaseViewModel(), GamePaginationListener {

  private var lastPage: GamePagination = GamePagination.DEFAULT

  private val gamesPageLiveData = MutableLiveData<AsyncResult<GamePagination>>(AsyncResult.Success(GamePagination.DEFAULT))
  fun getGamesPageLiveData(): LiveData<AsyncResult<GamePagination>> = gamesPageLiveData

  var isLoadingMoreGames = false

  init {
    requestTopGames()
  }

  private fun requestTopGames() {
    gamesPageLiveData.value?.let {
      if (it.isSuccess()) {
        requestMoreGames(it.data ?: GamePagination.DEFAULT)
      } else if (it.isError()) {
        requestMoreGames(lastPage)
      }
    }
  }

  private fun requestMoreGames(page: GamePagination) {
    viewModelScope.launch {
      isLoadingMoreGames = true

      if (!isReloading()) {
        val listWithLoadingItems = page.games + Game.LoadingGame().generateList(page.nextFewGames.orDefaultPage())
        gamesPageLiveData.value = AsyncResult.Success(page.copy(games = listWithLoadingItems))
      }

      val nextPageNumber = page.pageNumber + 1
      getTopGamesPaginationUseCase(nextPageNumber).collect { result ->
        result.getSuccessData()?.let { newPage ->
          val onlyLoadedGames = page.games.filterNot { it is Game.LoadingGame }
          val currentPage = page.copy(
            games = onlyLoadedGames + newPage.games,
            pageNumber = newPage.pageNumber,
            nextFewGames = newPage.nextFewGames
          )
          gamesPageLiveData.value = AsyncResult.Success(currentPage)
          lastPage = currentPage
          isLoadingMoreGames = false
          stopLoading()
        } ?: run {
          if (result.isError()) {
            gamesPageLiveData.value = result
            isLoadingMoreGames = false
            stopLoading()
          }
        }
      }
    }
  }

  override fun loadMoreItems() {
    requestTopGames()
  }

  override fun isLastPage() = gamesPageLiveData.value?.data?.nextFewGames == 0

  override fun isLoading() = isLoadingMoreGames

  override fun reload() {
    viewModelScope.launch {
      startLoading()
      delay(1000)
      requestTopGames()
    }
  }
}