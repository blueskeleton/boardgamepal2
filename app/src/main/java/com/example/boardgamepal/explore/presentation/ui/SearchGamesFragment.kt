package com.example.boardgamepal.explore.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.boardgamepal.common.presentation.listener.SearchListener
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.databinding.FragmentSearchGamesBinding
import com.example.boardgamepal.explore.presentation.viewmodel.SearchGamesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchGamesFragment : CommonBaseFragment() {

  private val viewModel: SearchGamesViewModel by viewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return FragmentSearchGamesBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      searchViewModel = viewModel
      searchListener = SearchListener(lifecycle) { viewModel.searchGame(it) }
    }.root
  }
}