package com.example.boardgamepal.explore.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.databinding.FragmentExploreBinding
import com.example.boardgamepal.explore.presentation.viewmodel.ExploreViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ExploreFragment : CommonBaseFragment() {

  private val viewModel: ExploreViewModel by viewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel

  override fun getMenuResource() = R.menu.explore_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return FragmentExploreBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      exploreViewModel = viewModel
    }.root
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_search_games -> {
        findNavController().navigate(ExploreFragmentDirections.actionGoToSearchGames())
        true
      }
      else -> false
    }
  }
}