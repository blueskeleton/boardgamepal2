package com.example.boardgamepal.explore.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.boardgamepal.common.data.repository.util.AsyncResult
import com.example.boardgamepal.common.data.repository.util.getSuccessData
import com.example.boardgamepal.common.domain.model.GamePagination
import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.usecases.api.GetSearchGamesUseCase
import com.example.boardgamepal.common.presentation.listener.GamePaginationListener
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.generateList
import com.example.boardgamepal.common.utils.extension.orDefaultPage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchGamesViewModel @Inject constructor(
  private val getSearchGamesUseCase: GetSearchGamesUseCase
) : CommonBaseViewModel(), GamePaginationListener {

  private var lastPage: GamePagination = GamePagination.DEFAULT

  private val searchGamesLiveData: MutableLiveData<AsyncResult<GamePagination>> = MutableLiveData()
  fun getSearchGamesLiveData(): LiveData<AsyncResult<GamePagination>> = searchGamesLiveData

  var currentQuery: String = ""
  var isLoadingMoreGames = false

  fun searchGame(query: String) {
    currentQuery = query
    requestMoreGames(GamePagination.DEFAULT)
  }

  private fun requestGames() {
    searchGamesLiveData.value?.let {
      if (it.isSuccess()) {
        requestMoreGames(it.data ?: GamePagination.DEFAULT)
      } else if (it.isError()) {
        requestMoreGames(lastPage)
      }
    }
  }

  private fun requestMoreGames(page: GamePagination) {
    viewModelScope.launch {
      isLoadingMoreGames = true

      if (!isReloading()) {
        val listWithLoadingItems = page.games + Game.LoadingGame().generateList(page.nextFewGames.orDefaultPage())
        searchGamesLiveData.value = AsyncResult.Success(page.copy(games = listWithLoadingItems))
      }

      val nextPageNumber = page.pageNumber + 1
      getSearchGamesUseCase(currentQuery, nextPageNumber).collect { result ->
        result.getSuccessData()?.let { newPage ->
          val onlyLoadedGames = page.games.filterNot { it is Game.LoadingGame }
          val currentPage = page.copy(
            games = onlyLoadedGames + newPage.games,
            pageNumber = newPage.pageNumber,
            nextFewGames = newPage.nextFewGames
          )
          searchGamesLiveData.value = AsyncResult.Success(currentPage)
          lastPage = currentPage
          isLoadingMoreGames = false
          stopLoading()
        } ?: run {
          if (result.isError()) {
            searchGamesLiveData.value = result
            isLoadingMoreGames = false
            stopLoading()
          }
        }

      }
    }
  }

  override fun loadMoreItems() {
    requestGames()
  }

  override fun isLastPage() = searchGamesLiveData.value?.data?.nextFewGames == 0

  override fun isLoading() = isLoadingMoreGames

  override fun reload() {
    viewModelScope.launch {
      startLoading()
      delay(1000)
      requestGames()
    }
  }
}