package com.example.boardgamepal.explore.presentation

import com.example.boardgamepal.common.domain.model.GamePagination

data class ExploreViewState(
  val loading: Boolean = true,
  val gamesPagination: GamePagination = GamePagination.DEFAULT,
  val noMoreGames: Boolean = false
)