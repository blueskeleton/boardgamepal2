package com.example.boardgamepal

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BoardGamePalApplication: Application()