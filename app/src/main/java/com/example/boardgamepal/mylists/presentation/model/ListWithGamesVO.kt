package com.example.boardgamepal.mylists.presentation.model

import com.example.boardgamepal.common.domain.model.game.Game
import com.example.boardgamepal.common.domain.model.list.GameList
import com.example.boardgamepal.common.domain.model.list.ListWithGames

data class ListWithGamesVO(
  val list: GameList,
  val games: List<Game>,
  val selected: Boolean = false
) {
  val hasExactlyOneGame: Boolean by lazy { games.size == 1 }
  val firstGameImage: String? by lazy { games.firstOrNull()?.cover?.small }
  val secondGameImage: String? by lazy { games.getOrNull(1)?.cover?.small }
  val thirdGameImage: String? by lazy { games.getOrNull(2)?.cover?.small }
  val fourthGameImage: String? by lazy { games.getOrNull(3)?.cover?.small }
}

fun List<ListWithGames>.toVO(): List<ListWithGamesVO> {
  return this.map { ListWithGamesVO(list = it.list, games = it.games) }
}

fun List<ListWithGamesVO>.selectList(listId: Long): List<ListWithGamesVO> {
  return this.map { it.copy(selected = it.list.id == listId) }
}

fun List<ListWithGamesVO>.deselectList(): List<ListWithGamesVO> {
  return this.map { it.copy(selected = false) }
}