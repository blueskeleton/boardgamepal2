package com.example.boardgamepal.mylists.presentation.ui

import android.os.Bundle
import android.text.InputType
import android.view.*
import androidx.fragment.app.activityViewModels
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.databinding.FragmentMyListsBinding
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsFiltersViewModel
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyListsFragment : CommonBaseFragment() {

  private lateinit var binding: FragmentMyListsBinding

  private val viewModel: MyListsViewModel by activityViewModels()
  private val filterViewModel: MyListsFiltersViewModel by activityViewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.my_lists_menu

  override fun getAboveSnackbarView() = activity?.findViewById<BottomNavigationView>(R.id.main_activity__view__bottom_nav)

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    binding = FragmentMyListsBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      listsViewModel = viewModel
    }

    return binding.root
  }

  override fun initializeView() {
    binding.fabButton.setOnClickListener {
      showCreateDialog()
    }
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)
    val showMenuItems = viewModel.isAnyListSelected()
    menu.findItem(R.id.action_edit)?.isVisible = showMenuItems
    menu.findItem(R.id.action_delete)?.isVisible = showMenuItems
    menu.findItem(R.id.action_deselect)?.isVisible = showMenuItems
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_edit -> {
        showUpdateDialog(viewModel.getSelectedListName())
        true
      }
      R.id.action_delete -> {
        showDeleteDialog()
        true
      }
      R.id.action_deselect -> {
        viewModel.deselectList()
        true
      }
      else -> super.onMenuItemSelected(item)
    }
  }

  private fun showCreateDialog() {
    MaterialDialog(requireContext()).show {
      title(R.string.create_a_list)
      message(R.string.you_can_edit_or_delete_list_later)
      input(
        hintRes = R.string.list_name_hint,
        inputType = InputType.TYPE_CLASS_TEXT,
        maxLength = 42) { _, inputText ->
          viewModel.createList(inputText.toString())
        }
      positiveButton(R.string.accept)
      negativeButton(R.string.cancel)
    }
  }

  private fun showUpdateDialog(currentName: String?) {
    MaterialDialog(requireContext()).show {
      title(R.string.update_name_list)
      input(
        hintRes = R.string.list_name_hint,
        inputType = InputType.TYPE_CLASS_TEXT,
        prefill = currentName,
        maxLength = 42) { _, inputText ->
          viewModel.updateSelectedList(inputText.toString())
          viewModel.deselectList()
        }
      positiveButton(R.string.accept)
      negativeButton(R.string.cancel)
    }
  }

  private fun showDeleteDialog() {
    MaterialDialog(requireContext()).show {
      title(R.string.are_you_sure_to_delete_list)
      message(R.string.this_will_delete_all_games_in_list)
      positiveButton(R.string.accept) {
        viewModel.deleteSelectedList()
        viewModel.deselectList()
      }
      negativeButton(R.string.cancel)
    }
  }
}