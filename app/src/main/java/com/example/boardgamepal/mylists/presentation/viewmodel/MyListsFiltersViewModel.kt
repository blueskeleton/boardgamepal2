package com.example.boardgamepal.mylists.presentation.viewmodel

import com.example.boardgamepal.common.presentation.utils.FilterListHelper
import com.example.boardgamepal.common.presentation.viewmodel.FilterListViewModel

class MyListsFiltersViewModel : FilterListViewModel() {

  private val savedFilters: MutableMap<Long, FilterListHelper> = mutableMapOf()

  fun initializeCurrentFilter(listId: Long) {
    setCurrentFilter(savedFilters.getOrElse(listId) {
      val filters = FilterListHelper()
      savedFilters[listId] = filters
      filters
    })
  }

  fun onExitSelectedList(listId: Long) {
    getCurrentFilterLiveData().value?.let { savedFilters[listId] = it }
  }

}