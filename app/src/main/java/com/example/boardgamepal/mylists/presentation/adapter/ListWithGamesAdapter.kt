package com.example.boardgamepal.mylists.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.boardgamepal.common.utils.extension.getLayoutInflater
import com.example.boardgamepal.databinding.RowListGameBinding
import com.example.boardgamepal.mylists.presentation.model.ListWithGamesVO
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsViewModel

class ListWithGamesAdapter(
  private val viewModel: MyListsViewModel
) : ListAdapter<ListWithGamesVO, GameListViewHolder>(GameListDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameListViewHolder {
    return GameListViewHolder(
      RowListGameBinding.inflate(parent.getLayoutInflater(), parent, false),
      viewModel
    )
  }

  override fun onBindViewHolder(holder: GameListViewHolder, position: Int) {
    holder.bind(getItem(position))
  }

}

class GameListViewHolder(
  private val binding: RowListGameBinding,
  private val viewModel: MyListsViewModel
) : RecyclerView.ViewHolder(binding.root) {
  fun bind(list: ListWithGamesVO) {
    binding.listWithGames = list
    binding.viewModel = viewModel
    binding.rowListLabelTitle.isSelected = true
  }
}

class GameListDiffCallback : DiffUtil.ItemCallback<ListWithGamesVO>() {
  override fun areItemsTheSame(oldItem: ListWithGamesVO, newItem: ListWithGamesVO): Boolean {
    return oldItem.list.id == newItem.list.id
  }

  override fun areContentsTheSame(oldItem: ListWithGamesVO, newItem: ListWithGamesVO): Boolean {
    return false
  }
}