package com.example.boardgamepal.mylists.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.boardgamepal.common.domain.usecases.gamelists.AddGameListUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.DeleteGameListUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.GetAllListsUseCase
import com.example.boardgamepal.common.domain.usecases.gamelists.UpdateGameListUseCase
import com.example.boardgamepal.common.presentation.utils.FilterListLocalSource
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.DELETE_LIST_ERROR
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.LIST_CREATED
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.LIST_DELETED
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.LIST_EMPTY
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.LIST_TWICE
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.LIST_UPDATED
import com.example.boardgamepal.common.presentation.utils.ScreenState.Companion.UPDATE_LIST_ERROR
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.presentation.vo.FilterVO
import com.example.boardgamepal.common.presentation.vo.SortVO
import com.example.boardgamepal.common.utils.extension.isTrue
import com.example.boardgamepal.common.utils.extension.sendEvents
import com.example.boardgamepal.mylists.presentation.model.ListWithGamesVO
import com.example.boardgamepal.mylists.presentation.model.deselectList
import com.example.boardgamepal.mylists.presentation.model.selectList
import com.example.boardgamepal.mylists.presentation.model.toVO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyListsViewModel @Inject constructor(
  private val getAllListsUseCase: GetAllListsUseCase,
  private val addGameListUseCase: AddGameListUseCase,
  private val updateGameListUseCase: UpdateGameListUseCase,
  private val deleteGameListUseCase: DeleteGameListUseCase
) : CommonBaseViewModel() {

  val filterListSource = FilterListLocalSource()

  private val gameListsLiveData = MutableLiveData<List<ListWithGamesVO>>()
  fun getGameListsLiveData(): LiveData<List<ListWithGamesVO>> = gameListsLiveData

  init {
    requestAllLists()
  }

  private fun requestAllLists() {
    viewModelScope.launch {
      getAllListsUseCase().collect {
        gameListsLiveData.value = it.toVO()
      }
    }
  }

  fun initializeListDetail(listId: Long, filters: List<FilterVO>, sort: SortVO) {
    filterListSource.setLoading()
    gameListsLiveData.value?.firstOrNull { it.list.id == listId }?.let {
      filterListSource.addFiltersAndSorting(filters, sort)
      filterListSource.setOriginalList(it.games)
    }
  }

  fun selectList(listId: Long): Boolean {
    gameListsLiveData.value?.let {
      gameListsLiveData.value = it.selectList(listId)
    }
    sendInvalidateMenuEvent()

    return isAnyListSelected()
  }

  fun deselectList() {
    gameListsLiveData.value?.let {
      gameListsLiveData.value = it.deselectList()
    }
    sendInvalidateMenuEvent()
  }

  fun isAnyListSelected(): Boolean {
    return gameListsLiveData.value?.any { it.selected }.isTrue()
  }

  fun getSelectedListName(): String? {
    return gameListsLiveData.value?.find { it.selected }?.list?.name
  }

  fun hasFilters(): Boolean {
    return filterListSource.hasFilters()
  }

  fun applyFilters(filters: List<FilterVO>, sort: SortVO) {
    filterListSource.addFiltersAndSorting(filters, sort)
    filterListSource.applyFilters()
  }

  fun removeFilters() {
    filterListSource.removeFilters()
  }

  fun onExitSelectedList() {
    filterListSource.removeFilters()
    filterListSource.setOriginalList(listOf())
  }

  fun createList(name: String?) {
    name?.takeIf { it.isNotBlank() }?.let {
      viewModelScope.launch {
        addGameListUseCase(it).collect {
          it.sendEvents(
            this@MyListsViewModel,
            LIST_CREATED,
            LIST_TWICE
          )
        }
      }
    } ?: run {
      sendErrorEvent(LIST_EMPTY)
    }
  }

  fun updateSelectedList(newName: String) {
    gameListsLiveData.value?.firstOrNull { it.selected }?.let {
      viewModelScope.launch {
        if (newName.isNotBlank()) {
          updateGameListUseCase(it.list.copy(name = newName)).let {
            it.sendEvents(
              this@MyListsViewModel,
              LIST_UPDATED,
              UPDATE_LIST_ERROR
            )
          }
        } else {
          sendErrorEvent(LIST_EMPTY)
        }
      }
    }
  }

  fun deleteSelectedList() {
    gameListsLiveData.value?.firstOrNull { it.selected }?.let {
      viewModelScope.launch {
        deleteGameListUseCase(it.list).let {
          it.sendEvents(
            this@MyListsViewModel,
            LIST_DELETED,
            DELETE_LIST_ERROR
          )
        }
      }
    }
  }

}