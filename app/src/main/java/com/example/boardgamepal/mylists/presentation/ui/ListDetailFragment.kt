package com.example.boardgamepal.mylists.presentation.ui;

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.ui.FilterListFragment
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.getFilterListForResult
import com.example.boardgamepal.common.utils.extension.invalidateMenu
import com.example.boardgamepal.databinding.FragmentListDetailBinding
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsFiltersViewModel
import com.example.boardgamepal.mylists.presentation.viewmodel.MyListsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListDetailFragment : CommonBaseFragment() {

  private val args: ListDetailFragmentArgs by navArgs()
  private val viewModel: MyListsViewModel by activityViewModels()
  private val filterViewModel: MyListsFiltersViewModel by activityViewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.filter_list_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    return FragmentListDetailBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      listDetailViewModel = viewModel
    }.root
  }

  override fun initializeView() {
    filterViewModel.initializeCurrentFilter(args.listId)
    viewModel.initializeListDetail(
      args.listId,
      filterViewModel.getFilters(),
      filterViewModel.getSorting()
    )
    getFilterListForResult()?.observe(viewLifecycleOwner) {
      if (it) {
        viewModel.applyFilters(filterViewModel.getFilters(), filterViewModel.getSorting())
        invalidateMenu()
      }
    }
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)
    menu.findItem(R.id.action_close)?.isVisible = viewModel.hasFilters()
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_filter_list -> {
        findNavController().navigate(ListDetailFragmentDirections.actionGoToFilterList(FilterListFragment.LIST))
        true
      }
      R.id.action_close -> {
        viewModel.removeFilters()
        filterViewModel.resetFilters()
        invalidateMenu()
        true
      }
      else -> false
    }
  }

  override fun onDestroyView() {
    viewModel.onExitSelectedList()
    filterViewModel.onExitSelectedList(args.listId)
    super.onDestroyView()
  }
}