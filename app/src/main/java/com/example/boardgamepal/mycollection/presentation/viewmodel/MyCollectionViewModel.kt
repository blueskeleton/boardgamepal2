package com.example.boardgamepal.mycollection.presentation.viewmodel

import androidx.lifecycle.viewModelScope
import com.example.boardgamepal.common.domain.usecases.collection.GetAllCollectionUseCase
import com.example.boardgamepal.common.presentation.utils.FilterListLocalSource
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.presentation.vo.FilterVO
import com.example.boardgamepal.common.presentation.vo.SortVO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyCollectionViewModel @Inject constructor(
  private val getAllCollectionUseCase: GetAllCollectionUseCase
) : CommonBaseViewModel() {

  val filterListSource = FilterListLocalSource()

  init {
    initializeCollection()
  }

  private fun initializeCollection() {
    viewModelScope.launch {
      filterListSource.setLoading()
      getAllCollectionUseCase().collect { result ->
        filterListSource.setOriginalList(result)
      }
    }
  }

  fun hasFilters(): Boolean {
    return filterListSource.hasFilters()
  }

  fun applyFilters(filters: List<FilterVO>, sort: SortVO) {
    filterListSource.addFiltersAndSorting(filters, sort)
    filterListSource.applyFilters()
  }

  fun removeFilters() {
    filterListSource.removeFilters()
  }

}