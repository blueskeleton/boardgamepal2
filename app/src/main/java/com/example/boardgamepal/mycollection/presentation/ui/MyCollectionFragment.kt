package com.example.boardgamepal.mycollection.presentation.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.ui.FilterListFragment.Companion.COLLECTION
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.getFilterListForResult
import com.example.boardgamepal.common.utils.extension.invalidateMenu
import com.example.boardgamepal.databinding.FragmentMyCollectionBinding
import com.example.boardgamepal.mycollection.presentation.viewmodel.MyCollectionFiltersViewModel
import com.example.boardgamepal.mycollection.presentation.viewmodel.MyCollectionViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyCollectionFragment : CommonBaseFragment() {

  private val viewModel: MyCollectionViewModel by viewModels()
  private val filterViewModel: MyCollectionFiltersViewModel by activityViewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.filter_list_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    return FragmentMyCollectionBinding.inflate(inflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      collectionViewModel = viewModel
    }.root
  }

  override fun initializeView() {
    getFilterListForResult()?.observe(viewLifecycleOwner) {
      if (it) {
        viewModel.applyFilters(
          filterViewModel.getFilters(),
          filterViewModel.getSorting()
        )
        invalidateMenu()
      }
    }
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)
    menu.findItem(R.id.action_close)?.isVisible = viewModel.hasFilters()
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_filter_list -> {
        findNavController().navigate(MyCollectionFragmentDirections.actionGoToFilterList(COLLECTION))
        true
      }
      R.id.action_close -> {
        viewModel.removeFilters()
        filterViewModel.resetFilters()
        invalidateMenu()
        true
      }
      else -> false
    }
  }
}