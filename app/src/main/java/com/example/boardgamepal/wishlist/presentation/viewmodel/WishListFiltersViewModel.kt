package com.example.boardgamepal.wishlist.presentation.viewmodel

import com.example.boardgamepal.common.presentation.viewmodel.FilterListViewModel

class WishListFiltersViewModel : FilterListViewModel()