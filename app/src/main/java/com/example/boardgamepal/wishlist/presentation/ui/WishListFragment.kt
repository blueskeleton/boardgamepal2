package com.example.boardgamepal.wishlist.presentation.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.boardgamepal.R
import com.example.boardgamepal.common.presentation.ui.CommonBaseFragment
import com.example.boardgamepal.common.presentation.ui.FilterListFragment.Companion.WISHLIST
import com.example.boardgamepal.common.presentation.viewmodel.CommonBaseViewModel
import com.example.boardgamepal.common.utils.extension.getFilterListForResult
import com.example.boardgamepal.common.utils.extension.invalidateMenu
import com.example.boardgamepal.databinding.FragmentWishlistBinding
import com.example.boardgamepal.wishlist.presentation.viewmodel.WishListFiltersViewModel
import com.example.boardgamepal.wishlist.presentation.viewmodel.WishListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WishListFragment : CommonBaseFragment() {

  private val viewModel: WishListViewModel by viewModels()
  private val filterViewModel: WishListFiltersViewModel by activityViewModels()

  override fun getCommonViewModel(): CommonBaseViewModel = viewModel
  override fun getMenuResource() = R.menu.filter_list_menu

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return FragmentWishlistBinding.inflate(layoutInflater, container, false).apply {
      lifecycleOwner = viewLifecycleOwner
      wishListViewModel = viewModel
    }.root
  }

  override fun initializeView() {
    getFilterListForResult()?.observe(viewLifecycleOwner) {
      if (it) {
        viewModel.applyFilters(filterViewModel.getFilters(), filterViewModel.getSorting())
        invalidateMenu()
      }
    }
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)
    menu.findItem(R.id.action_close)?.isVisible = viewModel.hasFilters()
  }

  override fun onMenuItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_filter_list -> {
        findNavController().navigate(WishListFragmentDirections.actionGoToFilterList(WISHLIST))
        true
      }
      R.id.action_close -> {
        viewModel.removeFilters()
        filterViewModel.resetFilters()
        invalidateMenu()
        true
      }
      else -> false
    }
  }

}